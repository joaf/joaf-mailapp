# README #

### What is this repository for? ###

* JOAF sample application - email gateway.
* Version 1.0-SNAPSHOT

### How do I get set up? ###

* make sure You are using java 8 (java --version)
* download and unpack Tomcat (7 or 8) http://tomcat.apache.org/
* compile and install (mvn install) latest version of joaf (current 1.1-SNAPSHOT, https://bitbucket.org/joaf/joaf branch: develop)
* start mysql database (config in env-vagrant, mysql port: 13306 - update Your configuration in mailapp.properties )
* create database and user (if You are using Your local database)
CREATE DATABASE `local_mailapp` CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL ON 'local_mailapp'.* TO 'user_mailapp'@'%' IDENTIFIED BY 'mailapppasword';
FLUSH PRIVILEGES;
* Copy context.xml and mailapp.properties to Your tomcat conf directory (${catalina_home}/conf)
* Import project to Your IDE (You can import both: joaf and joaf-mailapp)
* Run on server ... Tomcat
* Go to http://localhost:8080/mailapp/joafinstall/index.html
* Enter email and new password for administrator and click Install
* Restart tomcat server
* Go to http://localhost:8080/mailapp/ and log in.

### Contribution guidelines ###

### Who do I talk to? ###

* Cyprian Śniegota joaf@hmail.pl