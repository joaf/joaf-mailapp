package net.joaf.mailapp.appcard.model.enums;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public enum ESubjectAppAction {
	DETAILS, EDIT, TRASH, UNDOTRASH, SWITCH;
}
