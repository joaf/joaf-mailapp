package net.joaf.mailapp.appcard.model;

import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.db.StringUidEntity;
import net.joaf.base.subjectcard.model.SubjectCardData;

import java.io.Serializable;
import java.util.List;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public class SubjectAppCard implements SubjectCardData, StringUidEntity, Serializable {

	private String uid;
	private String name;
	private String shortName;
	private EObjectState objectState;
	private Boolean visible;
	private List<String> accessUsers;

	@Override
	public String getUid() {
		return uid;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getShortName() {
		return shortName;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public EObjectState getObjectState() {
		return objectState;
	}

	public void setObjectState(EObjectState objectState) {
		this.objectState = objectState;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public List<String> getAccessUsers() {
		return accessUsers;
	}

	public void setAccessUsers(List<String> accessUsers) {
		this.accessUsers = accessUsers;
	}
}
