<#import "/templates/base.ftl" as page />
<#import "/freemarker/joaf.ftl" as joaf />
<@page.layout "joaf.subjectcard.edit">
<div class="awidget full-width">
<#--    <div class="awidget-head">
<h3>
    </div>-->
    <div class="awidget-body">
        <form class="form-horizontal" role="form" method="post">
            <@joaf.formInput 'command.name' 'class="form-control" maxlength="50"' 'text' 'joaf.companyname.label' 'joaf.companyname.label'/>

            <@joaf.formInput 'command.shortName' 'class="form-control" maxlength="20"' 'text' 'joaf.companyshortname.label' 'joaf.companyshortname.label'/>

            <#assign placeholder = springMacroRequestContext.getMessage('joaf.taxnumber.label') />
            <@joaf.formInput 'command.vatNumber' 'placeholder="${placeholder}" class="form-control" maxlength="14"' 'text' 'joaf.taxnumber.label' />

            <div class="form-group">
                <label class="col-lg-3 control-label"><@spring.message 'joaf.address.label' /></label>

                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-4">
                            <label><@spring.message 'joaf.postalcode.label' /></label>
                            <@spring.formInput 'command.postCode' 'class="form-control" maxlength="6"' 'text' />
                            <#if spring.status.error>
                                <p><@spring.showErrors "<br>", "color:red" /></p>
                            </#if>
                        </div>
                        <div class="col-lg-8">
                            <label><@spring.message 'joaf.postoffice.label' /></label>
                            <@spring.formInput 'command.postCity' 'class="form-control" maxlength="50"' 'text' />
                            <#if spring.status.error>
                                <p><@spring.showErrors "<br>", "color:red" /></p>
                            </#if>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8">
                            <label><@spring.message 'joaf.street.label' /></label>
                            <@spring.formInput 'command.street' 'class="form-control" maxlength="50"' 'text' />
                            <#if spring.status.error>
                                <p><@spring.showErrors "<br>", "color:red" /></p>
                            </#if>
                        </div>
                        <div class="col-lg-4">
                            <label><@spring.message 'joaf.housenumber.label' /></label>
                            <@spring.formInput 'command.houseNumber' 'class="form-control" maxlength="6"' 'text' />
                            <#if spring.status.error>
                                <p><@spring.showErrors "<br>", "color:red" /></p>
                            </#if>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <hr/>
                <div class="col-lg-offset-2 col-lg-10">
                    <@spring.formHiddenInput 'command.uid' '' />
                    <button type="submit" class="btn btn-primary"><@spring.message 'joaf.save' /></button>
                    <button type="button"
                            onclick="this.form.action = '${rc.contextPath}/subjectcard/cancel/${command.uid}';this.form.submit();return false;"
                            class="btn btn-default"><@spring.message 'joaf.cancel' /></button>
                </div>
            </div>
        </form>
    </div>
</div>

</@page.layout>