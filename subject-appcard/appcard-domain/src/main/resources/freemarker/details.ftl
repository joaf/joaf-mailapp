<#import "/templates/base.ftl" as page />
<@page.layout "joaf.subjectcard.details">

<ul class="nav nav-tabs" id="myTab">
    <li class="active"><a data-toggle="tab" href="#home"><@spring.message 'joaf.subjectcard.details.main' /></a></li>
    <li><a data-toggle="tab" href="#account"><@spring.message 'joaf.subjectcard.details.access' /></a></li>
    <li><a href="${rc.contextPath}/subjectcard.html"><i
            class="fa fa-arrow-circle-up"></i> <@spring.message 'joaf.subjectcard.list' /></a></li>
</ul>
<div class="tab-content" id="tabContent">
    <div id="home" class="tab-pane fade in active">
        <#include "details_main.ftl" />
    </div>
    <div id="account" class="tab-pane fade">
        <#include "details_access.ftl" />
    </div>
</div>

</@page.layout>