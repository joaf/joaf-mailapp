package net.joaf.mailapp.appcard.services;

import com.google.common.collect.Sets;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.subjectcard.model.SubjectCardData;
import net.joaf.mailapp.appcard.model.SubjectAppCard;
import net.joaf.mailapp.appcard.repository.api.SubjectAppCardRepository;
import net.joaf.mainextensions.user.model.UserCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Service
public class SubjectAppCardService {

	@Autowired
	private SubjectAppCardRepository repository;

	public List<SubjectCardData> findForUser(String uid) throws JoafDatabaseException {
		return repository.findForUser(uid).stream().map(x -> (SubjectCardData) x).collect(Collectors.toList());
	}

	public SubjectCardData findByUid(String id) throws JoafDatabaseException {
		if (id == null) {
			return null;
		}
		Optional<SubjectAppCard> oneOptional = repository.findOneOptional(id);
		return oneOptional.isPresent() ? oneOptional.get() : null;
	}

	public void remove(String id) throws JoafDatabaseException {
		repository.remove(id);
	}

	public SubjectAppCard createNew(UserCard user) throws JoafDatabaseException {
		SubjectAppCard subjectAppCard = new SubjectAppCard();
		subjectAppCard.setVisible(true);
		subjectAppCard.setAccessUsers(Collections.singletonList(user.getUid()));
		subjectAppCard.setObjectState(EObjectState.NEW);
		subjectAppCard.setName("");
		subjectAppCard.setShortName("");
		subjectAppCard.setUid(repository.prepareId());
		repository.insert(subjectAppCard);
		return subjectAppCard;
	}

	public void cancel(String id) throws JoafDatabaseException {
		SubjectAppCard one = repository.findOne(id);
		if (EObjectState.NEW.equals(one.getObjectState())) {
			repository.remove(id);
		}
	}

	public void save(SubjectAppCard command) throws JoafDatabaseException {
		SubjectAppCard dbVersion = repository.findOne(command.getUid());
		if (EObjectState.NEW.equals(dbVersion.getObjectState())) {
			command.setObjectState(EObjectState.ACCEPTED);
		} else {
			command.setObjectState(dbVersion.getObjectState());
		}
		repository.store(command);
	}

	public Set<String> findSubjectRoles(String subjectCardDataId, String uid) throws JoafDatabaseException {
		if (subjectCardDataId == null) {
			return Sets.newHashSet();
		}
		String accessRoles = repository.findSubjectCardAccessRolesWithUser(subjectCardDataId, uid);
		return Sets.newHashSet(accessRoles.split(","));
	}
}
