package net.joaf.mailapp.appcard.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.mailapp.appcard.commands.handlers.TrashAppCardCommandHandler;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = TrashAppCardCommandHandler.class)
public class TrashAppCardCommand extends AbstractUidCommand implements Command {
    public TrashAppCardCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}