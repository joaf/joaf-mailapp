package net.joaf.mailapp.appcard.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailapp.appcard.commands.CancelAppCardCommand;
import net.joaf.mailapp.appcard.model.SubjectAppCard;
import net.joaf.mailapp.appcard.repository.api.SubjectAppCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class CancelAppCardCommandHandler implements CommandHandler<CancelAppCardCommand, Object> {

    @Autowired
    private SubjectAppCardRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CancelAppCardCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CancelAppCardCommand command) throws JoafException {
        SubjectAppCard element = repository.findOne(command.getElementUid());
        if (EObjectState.NEW.equals(element.getObjectState())) {
            repository.remove(command.getElementUid());
        }
        return null;
    }
}