package net.joaf.mailapp.appcard.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.mailapp.appcard.commands.handlers.UndoTrashAppCardCommandHandler;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = UndoTrashAppCardCommandHandler.class)
public class UndoTrashAppCardCommand extends AbstractUidCommand implements Command {
    public UndoTrashAppCardCommand(String elementUid, String userUid) {
        super(elementUid, userUid);
    }
}