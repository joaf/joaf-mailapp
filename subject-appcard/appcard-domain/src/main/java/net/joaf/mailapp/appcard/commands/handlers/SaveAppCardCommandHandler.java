package net.joaf.mailapp.appcard.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailapp.appcard.commands.SaveAppCardCommand;
import net.joaf.mailapp.appcard.model.SubjectAppCard;
import net.joaf.mailapp.appcard.repository.api.SubjectAppCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class SaveAppCardCommandHandler implements CommandHandler<SaveAppCardCommand, Object> {

    @Autowired
    private SubjectAppCardRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, SaveAppCardCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(SaveAppCardCommand command) throws JoafException {
        SubjectAppCard dbObject = repository.findOne(command.getElement().getUid());
//        command.getElement().setUpdated(LocalDateTime.now());
        repository.store(command.getElement());
        if (EObjectState.NEW.equals(dbObject.getObjectState())) {
            repository.updateObjectState(command.getElement().getUid(), EObjectState.ACCEPTED);
        }
        return null;
    }
}