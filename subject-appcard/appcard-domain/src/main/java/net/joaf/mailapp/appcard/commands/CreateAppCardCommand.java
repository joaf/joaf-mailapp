package net.joaf.mailapp.appcard.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidCommand;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.mailapp.appcard.commands.handlers.CreateAppCardCommandHandler;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = CreateAppCardCommandHandler.class)
public class CreateAppCardCommand extends AbstractUidCommand implements Command {
    public CreateAppCardCommand(String userUid) {
        super(null, userUid);
    }
}