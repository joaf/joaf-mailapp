package net.joaf.mailapp.appcard.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractEntityCommand;
import net.joaf.mailapp.appcard.commands.handlers.SaveAppCardCommandHandler;
import net.joaf.mailapp.appcard.model.SubjectAppCard;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandDto(handlerClass = SaveAppCardCommandHandler.class)
public class SaveAppCardCommand extends AbstractEntityCommand<SubjectAppCard> implements Command {
    public SaveAppCardCommand(SubjectAppCard element, String userUid) {
        super(element, userUid);
    }
}