package net.joaf.mailapp.appcard.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailapp.appcard.commands.CreateAppCardCommand;
import net.joaf.mailapp.appcard.model.SubjectAppCard;
import net.joaf.mailapp.appcard.repository.api.SubjectAppCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;
import java.util.Collections;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class CreateAppCardCommandHandler implements CommandHandler<CreateAppCardCommand, Object> {

    @Autowired
    private SubjectAppCardRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, CreateAppCardCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(CreateAppCardCommand command) throws JoafException {
        SubjectAppCard element = new SubjectAppCard();
        element.setVisible(true);
        element.setAccessUsers(Collections.singletonList(command.getUserUid()));
        element.setObjectState(EObjectState.NEW);
        element.setName("");
        element.setShortName("");
        element.setUid(repository.prepareId());
        repository.insert(element);
        return element;
    }
}