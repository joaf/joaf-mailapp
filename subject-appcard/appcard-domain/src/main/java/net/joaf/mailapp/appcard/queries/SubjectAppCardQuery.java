package net.joaf.mailapp.appcard.queries;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mailapp.appcard.model.SubjectAppCard;
import net.joaf.mailapp.appcard.repository.api.SubjectAppCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Service
public class SubjectAppCardQuery {

	@Autowired
	private SubjectAppCardRepository repository;

	public SubjectAppCard findOne(String uid) throws JoafDatabaseException {
		return repository.findOne(uid);
	}

	public List<SubjectAppCard> findForUser(String userUid) throws JoafDatabaseException {
		return repository.findForUser(userUid);
	}
}
