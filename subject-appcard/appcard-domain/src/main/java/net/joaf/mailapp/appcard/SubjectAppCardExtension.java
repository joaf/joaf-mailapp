package net.joaf.mailapp.appcard;

import net.joaf.base.extension.AbstractExtension;
import net.joaf.base.extension.JoafExtension;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 07.03.15.
 */

@Component
public class SubjectAppCardExtension extends AbstractExtension implements JoafExtension {

    private static final String EXTENSION_METADATA_FILE = "/extension-subjectappcard.xml";

    @Override
    public String getExtensionMetadataFile() {
        return EXTENSION_METADATA_FILE;
    }
}
