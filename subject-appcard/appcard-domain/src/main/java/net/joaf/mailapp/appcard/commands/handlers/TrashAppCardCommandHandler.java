package net.joaf.mailapp.appcard.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailapp.appcard.commands.TrashAppCardCommand;
import net.joaf.mailapp.appcard.repository.api.SubjectAppCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 03.04.15.
 */
@CommandHandlerComponent
public class TrashAppCardCommandHandler implements CommandHandler<TrashAppCardCommand, Object> {

    @Autowired
    private SubjectAppCardRepository repository;

    @Override
    public BindingResult validate(BindingResult originalBindingResult, TrashAppCardCommand command) throws JoafException {
        return originalBindingResult;
    }

    @Override
    public Object execute(TrashAppCardCommand command) throws JoafException {
        repository.updateObjectState(command.getElementUid(), EObjectState.TRASH);
        return null;
    }
}