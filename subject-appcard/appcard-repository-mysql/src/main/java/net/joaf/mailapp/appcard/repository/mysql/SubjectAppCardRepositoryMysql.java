package net.joaf.mailapp.appcard.repository.mysql;

import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mailapp.appcard.model.SubjectAppCard;
import net.joaf.mailapp.appcard.repository.api.SubjectAppCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Repository
public class SubjectAppCardRepositoryMysql extends AbstractRepositoryMysql<SubjectAppCard> implements SubjectAppCardRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;


	@Override
	protected String getTableName() {
		return "appcard";
	}

	@Override
	protected JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	protected RowMapper<SubjectAppCard> getRowMapper() {
		return (resultSet, i) -> {
            SubjectAppCard element = new SubjectAppCard();
            element.setUid(resultSet.getString("uid"));
            element.setName(resultSet.getString("name"));
            element.setShortName(resultSet.getString("name"));
            // TODO: access users
            element.setAccessUsers(Collections.emptyList());
            String objectState = resultSet.getString("objectState");
            if (objectState != null) {
                element.setObjectState(EObjectState.valueOf(objectState));
            }
            element.setVisible(resultSet.getBoolean("visible"));
            return element;
        };
	}

	@Override
	public List<SubjectAppCard> findForUser(String userUid) throws JoafDatabaseException {
		String query = "SELECT m.* FROM "+this.getTableName()+" m left join usercard_appcard ua ON (m.uid = ua.subjectcarduid) WHERE ua.usercarduid = ? AND m.objectState = ?";
		return jdbcTemplate.query(query, new Object[] {userUid, EObjectState.ACCEPTED.toString()}, getRowMapper());
	}

	@Override
	public String findSubjectCardAccessRolesWithUser(String subjectCardDataId, String uid) {
		String query = "SELECT userroles FROM usercard_appcard ua WHERE ua.subjectcarduid = ? AND ua.usercarduid = ?";
		return jdbcTemplate.queryForObject(query, new Object[] { subjectCardDataId, uid }, String.class);
	}

	@Override
	public void insert(SubjectAppCard subjectAppCard) throws JoafDatabaseException {
		String query = "INSERT INTO "+getTableName()+" (uid, `name`, objectState, visible) VALUES (?, ?, ?, ?)";
		jdbcTemplate.update(query, subjectAppCard.getUid(), subjectAppCard.getName(), subjectAppCard.getObjectState().toString(), subjectAppCard.getVisible());
		query = "INSERT INTO usercard_appcard (usercarduid, subjectcarduid, userroles, username, subjectname) VALUES (?, ?, ?, ?, ?)";
		jdbcTemplate.update(query, subjectAppCard.getAccessUsers().iterator().next(), subjectAppCard.getUid(), "ROLE_SUBJECT_OWNER", "", subjectAppCard.getShortName());
	}

	@Override
	public void store(SubjectAppCard subjectAppCard) throws JoafDatabaseException {
		String query = "UPDATE "+getTableName()+" SET `name` = ?, visible = ? WHERE uid = ?";
		jdbcTemplate.update(query, subjectAppCard.getName(), subjectAppCard.getVisible(), subjectAppCard.getUid());
	}

	@Override
	public String collectionName() {
		return getTableName();
	}

	@Override
	public Class<SubjectAppCard> getEntityClass() {
		return SubjectAppCard.class;
	}

	@Override
	public String prepareId() {
		return UUID.randomUUID().toString();
	}
}
