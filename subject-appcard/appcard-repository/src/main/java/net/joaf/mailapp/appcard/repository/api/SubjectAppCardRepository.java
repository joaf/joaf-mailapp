package net.joaf.mailapp.appcard.repository.api;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.NoSqlRepository;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mailapp.appcard.model.SubjectAppCard;

import java.util.List;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public interface SubjectAppCardRepository extends NoSqlRepository, CrudRepository<SubjectAppCard> {

	List<SubjectAppCard> findForUser(String userUid) throws JoafDatabaseException;

	String findSubjectCardAccessRolesWithUser(String subjectCardDataId, String uid);
}
