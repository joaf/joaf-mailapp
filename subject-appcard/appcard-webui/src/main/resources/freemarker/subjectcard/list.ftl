<#import "/templates/base.ftl" as page />
<@page.layout "joaf.subject.appcard.list">


<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
            <a href="${rc.contextPath}/subjectcard/create.html"
               class="btn btn-primary"><@spring.message 'joaf.create' /></a>
        <#--<button class="btn btn-success">Center</button>-->
        <#--<button class="btn btn-primary">Right</button>-->
        </div>
    </div>
    <div class="awidget-body">
        <table class="table admin-media table-hover">
            <thead>
            <tr>
                <th>
                    <input type='checkbox' value='check1'/>
                </th>
                <th><@spring.message 'joaf.name.label'/></th>
                <th></th>
            </tr>
            </thead>
            <tbody>

                <#list elements as element>
                    <#assign rowClass="" />

                    <#if selected_element??>
                        <#if selected_element.uid == element.data.uid>
                            <#assign rowClass="success" />
                        </#if>
                    </#if>

                <tr class="${rowClass}">
                    <td>
                        <input type='checkbox' value='check[]'/>
                    </td>
                    <td>${element.data.name}</td>
                    <td>
                        <div class="hidden-table-cell">
                            <#list element.webActions as action>
                                <#if action.requireConfirmation>
                                    <a href="#removeSubjectModal" class="btn btn-xs ${action.buttonStyle}"
                                       data-toggle="modal" rel="tooltip" data-placement="bottom"
                                       title="<@spring.message action.tooltip />"
                                       onclick="updateModal('${rc.contextPath}${action.actionUrl}','<@spring.message action.confirmationText />');"><span aria-hidden="true"
                                                                                                                                                          class="glyphicon ${action.iconName}"></span></a>
                                <#else>
                                    <a href="${rc.contextPath}${action.actionUrl}" class="btn btn-xs ${action.buttonStyle}"
                                       data-toggle="tooltip" data-placement="bottom"
                                       title="<@spring.message action.tooltip />"><span aria-hidden="true"
                                                                                        class="glyphicon ${action.iconName}"></span></a>
                                </#if>
                            </#list>
                        </div>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>

</div>
<#--okna potwierdzenia-->
<script>
    var updateModal = function (url, msg) {
        jQuery("#modalUrl").attr("href", url);
        jQuery("#modalMsg").html(msg);
    }
</script>
<#--modal usuwania firmy-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" class="modal fade"
     id="removeSubjectModal" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                <h4 class="modal-title"><@spring.message 'joaf.modal.confirmation.header' /></h4>
            </div>
            <div class="modal-body">
                <p id="modalMsg">???</p>
            </div>
            <div class="modal-footer">
                <button aria-hidden="true" data-dismiss="modal" class="btn btn-default"
                        type="button"><@spring.message 'joaf.close' /></button>
                <a id="modalUrl" href="${rc.contextPath}" class="btn btn-danger"
                   type="button"><@spring.message 'joaf.remove' /></a>
            </div>
        </div>
    </div>
</div>
</@page.layout>