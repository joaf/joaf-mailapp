<#import "/templates/base.ftl" as page />
<#import "/freemarker/joaf.ftl" as joaf />
<@page.layout "joaf.subjectcard.edit">
<div class="awidget full-width">
<#--    <div class="awidget-head">
<h3>
    </div>-->
    <div class="awidget-body">
        <form class="form-horizontal" role="form" method="post">

            <@joaf.formInput 'command.name' 'class="form-control" maxlength="50"' 'text' 'joaf.name.label' 'joaf.name.label'/>

            <div class="form-group">
                <hr/>
                <div class="col-lg-offset-2 col-lg-10">
                    <@spring.formHiddenInput 'command.uid' '' />
                    <button type="submit" class="btn btn-primary"><@spring.message 'joaf.save' /></button>
                    <button type="button"
                            onclick="this.form.action = '${rc.contextPath}/subjectcard/cancel/${command.uid}';this.form.submit();return false;"
                            class="btn btn-default"><@spring.message 'joaf.cancel' /></button>
                </div>
            </div>
        </form>
    </div>
</div>

</@page.layout>