/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mailapp.appcard.webui.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.base.subjectcard.model.SubjectCardData;
import net.joaf.mailapp.appcard.commands.*;
import net.joaf.mailapp.appcard.commands.handlers.*;
import net.joaf.mailapp.appcard.model.SubjectAppCard;
import net.joaf.mailapp.appcard.model.enums.ESubjectAppAction;
import net.joaf.mailapp.appcard.queries.SubjectAppCardQuery;
import net.joaf.mailapp.appcard.services.SubjectAppCardService;
import net.joaf.mailapp.appcard.webui.helpers.SubjectCardControllerHelper;
import net.joaf.mainextensions.user.model.BasicUserDetails;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.ws.rs.QueryParam;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 14.03.14.
 */

@Controller
@RequestMapping("/subjectcard")
public class SubjectAppCardController {

	private static final Logger logger = LoggerFactory.getLogger(SubjectAppCardController.class);
	private static final String EXTENSION_BASE_PATH = "/subjectcard";

	@Autowired
	private CommandGateway commandGateway;

	@Autowired
	private SubjectAppCardQuery query;

	@RequestMapping
	public String list(Map<String, Object> model, Principal principal, HttpSession session) {
		SubjectCardData subjectCard = this.getSubjectCard(session);
		model.put("selected_element", subjectCard);
		try {
			Authentication authentication = (Authentication) principal;
			BasicUserDetails userDetails = (BasicUserDetails) authentication.getPrincipal();
			List<SubjectAppCard> elements = query.findForUser(userDetails.getUser().getUid());
			List<WrapperWDTO<SubjectAppCard>> displayList = elements.stream().map(SubjectCardControllerHelper::createWrapper)
					.collect(Collectors.<WrapperWDTO<SubjectAppCard>>toList());
			displayList.forEach((x) -> {
				x.getWebActions().add(SubjectCardControllerHelper.createActionDetails(ESubjectAppAction.DETAILS, x.getData().getUid()));
				x.getWebActions().add(SubjectCardControllerHelper.createActionDetails(ESubjectAppAction.EDIT, x.getData().getUid()));
				x.getWebActions().add(SubjectCardControllerHelper.createActionDetails(ESubjectAppAction.TRASH, x.getData().getUid()));
				if (subjectCard == null || !subjectCard.getUid().equals(x.getData().getUid())) {
					x.getWebActions().add(SubjectCardControllerHelper.createActionDetails(ESubjectAppAction.SWITCH, x.getData().getUid()));
				}
			});
			model.put("elements", displayList);
		} catch (JoafDatabaseException e) {
			logger.error("Error in controller", e);
		}
		return "freemarker/subjectcard/list";
	}

	@RequestMapping("/edit/{id}")
	public String edit(@PathVariable("id") String id, HttpSession session, Model model) {
		SubjectAppCard command;
		try {
			command = query.findOne(id);
			model.addAttribute("command", command);
		} catch (JoafDatabaseException e) {
			logger.error("Error in controller", e);
		}
		return getViewitem("/edit");
	}

	@RequestMapping("/create")
	public String create(@QueryParam("autoredirected") Boolean autoredirected, HttpSession session, Model model, Principal principal, RedirectAttributes redirectAttributes) {

		SessionContext sessionContext = SessionContext.loadFromSession(session);
		String userUid = getUserUid(principal);
		try {
			SubjectAppCard command = (SubjectAppCard) commandGateway.send(new CreateAppCardCommand(userUid), CreateAppCardCommandHandler.class);
			if (autoredirected != null && autoredirected) {
				WebFlashUtil.addMessage(redirectAttributes, "net.joaf.appcard.noappcard.message.title", "net.joaf.appcard.noappcard.message.body");
			}
			return getRedirect("/edit/" + command.getUid());
		} catch (Exception e) {
			logger.debug("error", e);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
			return getRedirect("");
		}
	}

	@RequestMapping("/cancel/{uid}")
	public String cancel(@PathVariable("uid") String uid, HttpSession session, Principal principal, Model model) {
		String userUid = getUserUid(principal);
		try {
			commandGateway.send(new CancelAppCardCommand(uid, userUid), CancelAppCardCommandHandler.class);
		} catch (Exception e) {
			logger.error("", e);
			WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
		}
		return getRedirect("");
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String save(@PathVariable("id") String id, @Valid @ModelAttribute("command") SubjectAppCard command, BindingResult result, Model model,
			Principal principal, HttpSession session, final RedirectAttributes redirectAttributes) {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		String userUid = getUserUid(principal);
		try {
			commandGateway.send(new SaveAppCardCommand(command, userUid), SaveAppCardCommandHandler.class);
			String subjectCardDataId = sessionContext.getSubjectCardDataId();
			if (subjectCardDataId == null) {
				sessionContext.setSubjectCardDataId(command.getUid());
			}
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave");
			return getRedirect("/details/" + id);
		} catch (Exception e) {
			logger.error("", e);
			WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
			return getViewitem("/edit");
		}
	}

	@RequestMapping("/trash/{uid}")
	public String trash(@PathVariable("uid") String uid, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		String userUid = getUserUid(principal);
		try {
			commandGateway.send(new TrashAppCardCommand(uid, userUid), TrashAppCardCommandHandler.class);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successtrash", httpRequest.getContextPath() + getUrl("/undotrash/" + uid));
		} catch (Exception e) {
			logger.error("", e);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
		}
		return getRedirect("");
	}

	@RequestMapping("/undotrash/{id}")
	public String undotrash(@PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		String userUid = getUserUid(principal);
		try {
			commandGateway.send(new UndoTrashAppCardCommand(id, userUid), UndoTrashAppCardCommandHandler.class);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successundotrash", "1");
		} catch (Exception e) {
			logger.error("", e);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
		}
		return getRedirect("");
	}

	@RequestMapping("/switch/{uid}")
	public String switchSubject(@PathVariable("uid") String uid, @QueryParam(value = "ret") String ret, HttpSession session) {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		if (StringUtils.trimToNull(uid) != null) {
			sessionContext.setSubjectCardDataId(uid);
		}
		List<String> availableReturns = Collections.singletonList("subjectcard");
		if (ret != null && availableReturns.contains(ret)) {
			return "redirect:/" + ret + ".html";
		}
		return "redirect:/subjectcard/details/" + uid + ".html";
	}

	@RequestMapping("/details/{id}")
	public String details(@PathVariable("id") String id, HttpSession session, Model model) {
		SubjectAppCard command;
		try {
			command = query.findOne(id);
			model.addAttribute("command", command);
		} catch (JoafDatabaseException e) {
			logger.error("Error in controller", e);
		}
		return getViewitem("/details");
	}

	private String getUserUid(Principal principal) {
		return ((BasicUserDetails) ((UsernamePasswordAuthenticationToken) principal).getPrincipal()).getUser().getUid();
	}

	public SubjectCardData getSubjectCard(HttpSession session) {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		if (sessionContext != null && sessionContext.getSubjectCardData() != null) {
			return sessionContext.getSubjectCardData();
		}
		return null;
	}

	private String getViewitem(String subPath) {
		return "freemarker" + EXTENSION_BASE_PATH + StringUtils.trimToEmpty(subPath);
	}

	protected String getRedirect(String name) {
		return "redirect:" + getUrl(name);
	}

	protected String getUrl(String name) {
		return EXTENSION_BASE_PATH + name + ".html";
	}

}
