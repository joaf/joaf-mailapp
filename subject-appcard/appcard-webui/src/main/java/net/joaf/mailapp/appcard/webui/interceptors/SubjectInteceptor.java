/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mailapp.appcard.webui.interceptors;

import net.joaf.base.core.session.SessionContext;
import net.joaf.base.subjectcard.model.SubjectCardData;
import net.joaf.mailapp.appcard.model.SubjectAppCard;
import net.joaf.mailapp.appcard.services.SubjectAppCardService;
import net.joaf.mainextensions.user.model.BasicUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.text.html.Option;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * User: Cyprian.Sniegota
 * Date: 21.10.13
 * Time: 12:36
 * To change this template use File | Settings | File Templates.
 */
@Component
public class SubjectInteceptor implements HandlerInterceptor {

	@Autowired
	private SubjectAppCardService service;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if (request.getPathInfo().contains(".css") || request.getPathInfo().contains(".js") || request.getPathInfo().contains(".png") || request.getPathInfo().contains("oauth")) {
			return true;
		}
		Boolean contextPrepared = false;
		HttpSession session = request.getSession();
		//Object spring_security_context = session.getAttribute("SPRING_SECURITY_CONTEXT");  && spring_security_context instanceof SecurityContext
		Object spring_security_context = SecurityContextHolder.getContext();
		if (spring_security_context != null) {
			SecurityContext securityContext = (SecurityContext) spring_security_context;
			Authentication authentication = securityContext.getAuthentication();
			if (authentication == null) {
				return true;
			}
			BasicUserDetails userDetails = (BasicUserDetails) authentication.getPrincipal();
			SessionContext sessionContext = SessionContext.loadFromSession(session);
			if (sessionContext == null) {
				sessionContext = new SessionContext();
				sessionContext.saveToSession(session);
			} else {
				contextPrepared = true;
			}
			if (sessionContext.getSubjectCardDataId() == null && !contextPrepared) {
				//TODO: load user default subject
				List<SubjectCardData> subjects = service.findForUser(userDetails.getUser().getUid());
				if (subjects.size() > 0) {
					sessionContext.setSubjectCardDataId(subjects.get(0).getUid());
				}
			}
			SubjectCardData subjectCard = service.findByUid(sessionContext.getSubjectCardDataId());
			if (subjectCard == null) {
				sessionContext.setSubjectCardDataId(null);
			} else {
				Set<String> subjectRoles = service.findSubjectRoles(sessionContext.getSubjectCardDataId(), userDetails.getUser().getUid());

				Set<GrantedAuthority> authorities = new HashSet<>();
				// user roles
				authorities.addAll(userDetails.getUser().getEffectiveRoles().stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet()));
				// roles in subject
				authorities.addAll(subjectRoles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet()));
				if (authentication.getAuthorities().size() != authorities.size()) {
					authentication = new UsernamePasswordAuthenticationToken(userDetails, "", authorities);
					SecurityContextHolder.getContext().setAuthentication(authentication);
				}
			}
			sessionContext.setSubjectCardData(subjectCard);

		}
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		//To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		//To change body of implemented methods use File | Settings | File Templates.
	}
}
