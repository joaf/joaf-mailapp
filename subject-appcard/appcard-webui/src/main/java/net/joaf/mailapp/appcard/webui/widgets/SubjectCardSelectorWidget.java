/*
 * Licensed to the HMail.pl under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The HMail.pl licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.joaf.mailapp.appcard.webui.widgets;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.extension.Widget;
import net.joaf.base.extension.exceptions.WidgetException;
import net.joaf.base.extension.model.WidgetMAV;
import net.joaf.base.subjectcard.model.SubjectCardData;
import net.joaf.mailapp.appcard.services.SubjectAppCardService;
import net.joaf.mainextensions.user.model.BasicUserDetails;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

/**
 * Created by cyprian on 02.05.14.
 */
@Component
public class SubjectCardSelectorWidget implements Widget, ApplicationContextAware {

	@Autowired
	private FreeMarkerConfigurer freeMarkerConfigurer;

	@Autowired
	private SubjectAppCardService service;

	private ApplicationContext applicationContext;

	@Override
	public boolean availableForUserAndSubject(String username, String subjectId) {
		return StringUtils.trimToNull(username) != null;
	}

	@Override
	public WidgetMAV process(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws WidgetException {
		try {
			String fileName = "freemarker/widget/subjectCardSelector.ftl";
			WidgetMAV widgetMAV = new WidgetMAV(fileName);
			Map<String, Object> dataModel = widgetMAV.getModel();
			List<SubjectCardData> subjects = this.getUserSubjectCards(request);
			dataModel.put("joaf_user_subjects", subjects);
			SubjectCardData selectedCompamy = this.getUserSelectedSubject(request);
			dataModel.put("joaf_user_selected_company", selectedCompamy);
			return widgetMAV;
		} catch (JoafDatabaseException e) {
			throw new WidgetException("Error widget processing", e);
		}

	}

	@Override
	public String getForm() {
		return null;
	}

	private List<SubjectCardData> getUserSubjectCards(HttpServletRequest request) throws JoafDatabaseException {
		Authentication authentication = this.getAuthentication(request);
		if (authentication != null) {
			BasicUserDetails userDetails = (BasicUserDetails) authentication.getPrincipal();
			//			log.debug("Company list for user ["+authentication.getName()+"] = ["+companiesForUser+"]");
			return service.findForUser(userDetails.getUser().getUid());
		} else {
			//            log.debug("authentication not found, no user companies.");
			return null;
		}
	}

	private SubjectCardData getUserSelectedSubject(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Authentication authentication = this.getAuthentication(request);
		if (authentication != null) {
			SessionContext companyContext = SessionContext.loadFromSession(session);
			if (companyContext != null) {
				return companyContext.getSubjectCardData();
			}
		}
		return null;
	}

	private Authentication getAuthentication(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Object spring_security_context = session.getAttribute("SPRING_SECURITY_CONTEXT");
		if (spring_security_context != null && spring_security_context instanceof SecurityContext) {
			SecurityContext securityContext = (SecurityContext) spring_security_context;
			//            log.debug("found authentication: ["+ authentication +"] ["+authentication.getName()+"]");
			return securityContext.getAuthentication();
		} else {
			//            log.debug("authentication not found");
			return null;
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
