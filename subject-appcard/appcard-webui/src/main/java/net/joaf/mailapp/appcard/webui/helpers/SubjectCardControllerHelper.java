/*
 * Licensed to the HMail.pl under one or more contributor license agreements.  See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership. The HMail.pl licenses this file to You under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES
 * OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package net.joaf.mailapp.appcard.webui.helpers;

import net.joaf.base.core.web.model.WebActionDetails;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.mailapp.appcard.model.SubjectAppCard;
import net.joaf.mailapp.appcard.model.enums.ESubjectAppAction;

/**
 * Helper class for Controller
 * Created by cyprian on 01.05.14.
 */
public class SubjectCardControllerHelper {

	public static WebActionDetails createActionDetails(ESubjectAppAction action, String uid) {
		WebActionDetails webActionDetails = new WebActionDetails();
		switch (action) {
		case EDIT:
			webActionDetails.setButtonStyle("btn-primary");
			webActionDetails.setActionUrl("/subjectcard/edit/" + uid + ".html");
			webActionDetails.setIconName("glyphicon-edit");
			webActionDetails.setTooltip("joaf.tooltip.edit");
			break;
		case TRASH:
			webActionDetails.setButtonStyle("btn-danger");
			webActionDetails.setActionUrl("/subjectcard/trash/" + uid + ".html");
			webActionDetails.setIconName("glyphicon-trash");
			webActionDetails.setTooltip("joaf.tooltip.trash");
			webActionDetails.setConfirmationText("joaf.tooltip.subject.trashconfirmation");
			webActionDetails.setRequireConfirmation(true);
			break;
		case DETAILS:
			webActionDetails.setButtonStyle("btn-info");
			webActionDetails.setActionUrl("/subjectcard/details/" + uid + ".html");
			webActionDetails.setIconName("glyphicon-eye");
			webActionDetails.setTooltip("joaf.tooltip.details");
			break;
		case SWITCH:
			webActionDetails.setButtonStyle("btn-info");
			webActionDetails.setActionUrl("/subjectcard/switch/" + uid + ".html?ret=subjectcard");
			webActionDetails.setIconName("fa-check-square-o");
			webActionDetails.setTooltip("joaf.tooltip.switch");
			break;
		default:
			webActionDetails = null;
			break;
		}
		return webActionDetails;
	}

	public static WrapperWDTO<SubjectAppCard> createWrapper(SubjectAppCard data) {
		return new WrapperWDTO<>(data);
	}

}
