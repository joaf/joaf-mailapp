#!/bin/bash
docker stop joaf_elasticsearch
docker rm joaf_elasticsearch
docker run --name joaf_elasticsearch -d -p 9200:9200 -p 9300:9300 elasticsearch:1.5

