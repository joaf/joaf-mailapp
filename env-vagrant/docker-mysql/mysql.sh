#!/bin/bash
docker stop joaf_mysql
docker rm joaf_mysql
docker run --name joaf_mysql -e MYSQL_ROOT_PASSWORD=mysecretpassword -e MYSQL_USER=user_mailapp -e MYSQL_PASSWORD=mailapppassword -e MYSQL_DATABASE=local_mailapp -d -p 3306:3306 mysql

