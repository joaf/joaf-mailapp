#!/usr/bin/env bash
# system & docker
# wget http://download.virtualbox.org/virtualbox/4.3.22/VBoxGuestAdditions_4.3.22.iso
# mkdir /mnt/vbox
# mount -o loop VBoxGuestAdditions_4.3.22.iso /mnt/vbox
# cd /mnt/vbox && run vbox linux installer
whoami
groupadd docker
yum -y update
yum -y install git
yum -y install vim
yum groupinstall "Development Tools"
yum install kernel-devel
yum -y install docker
wget https://get.docker.com/builds/Linux/x86_64/docker-latest -O docker
chmod +x docker
mv docker /usr/bin/docker
git clone https://github.com/docker/docker.git
cd docker/contrib/init/systemd/
cp docker.* /etc/systemd/system/
chkconfig docker on
mkdir /etc/systemd/system/docker.service.d
echo "[Service]" >>/etc/systemd/system/docker.service.d/http-proxy.conf
echo "Environment=\"HTTP_PROXY_CHANGEME=http://proxy.example.com:8080\"" >>/etc/systemd/system/docker.service.d/http-proxy.conf
systemctl daemon-reload
systemctl start docker
cd /vagrant/docker-mysql/
./mysql.sh
cd ../docker-activemq/
./activemq.sh
cd ../docker-elasticsearch/
./elasticsearch.sh

