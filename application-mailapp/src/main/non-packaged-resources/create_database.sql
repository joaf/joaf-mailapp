CREATE DATABASE local_mailapp CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL ON local_mailapp.* TO user_mailapplocalhost IDENTIFIED BY 'mailapppasword'; 
FLUSH PRIVILEGES;