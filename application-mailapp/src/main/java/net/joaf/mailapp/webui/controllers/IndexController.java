package net.joaf.mailapp.webui.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Controller
public class IndexController {

	@RequestMapping({"/","/index"})
	public String index() {
		return "redirect:/mailmessage/queue/dashboard.html";
	}

}
