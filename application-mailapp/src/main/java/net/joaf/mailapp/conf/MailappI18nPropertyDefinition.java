package net.joaf.mailapp.conf;

import net.joaf.base.language.utils.I18nPropertyDefinition;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by cyprian on 15.03.15.
 */
@Component
public class MailappI18nPropertyDefinition implements I18nPropertyDefinition {
    @Override
    public List<String> propertyFiles() {
        return Collections.singletonList("classpath:i18n/mailapp");
    }
}
