package net.joaf.mailmessage.repository.api;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.NoSqlRepository;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mailmessage.model.MailMessageTemplate;

/**
 * Repository for mail message template
 *
 * @author Cyprian Śniegota
 * @since 1.0
 */
public interface MailMessageTemplateRepository extends NoSqlRepository, CrudRepository<MailMessageTemplate> {

	MailMessageTemplate findByAlias(String subjectUid, String messageAlias) throws JoafDatabaseException;
}
