package net.joaf.mailmessage.repository.api;

import net.joaf.base.core.db.CrudRepository;
import net.joaf.base.core.db.NoSqlRepository;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mailmessage.model.MailMessage;
import net.joaf.mailmessage.model.MailMessageParsed;
import net.joaf.mailmessage.model.enums.EMessageState;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public interface MailMessageRepository extends NoSqlRepository, CrudRepository<MailMessage> {
	void updateMessageState(String uid, EMessageState newState) throws JoafDatabaseException;

	void updateParsed(String uid, MailMessageParsed parsed) throws JoafDatabaseException;

	void updateError(String uid, String localizedMessage) throws JoafDatabaseException;
}
