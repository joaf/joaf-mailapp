package net.joaf.mailmessage.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public class MailMessageBulkRequest implements Serializable {

	private List<MailMessageRequest> requests = new ArrayList<>();
	private MailMessageRequest commons = new MailMessageRequest();

	public List<MailMessageRequest> getRequests() {
		return requests;
	}

	public void setRequests(List<MailMessageRequest> requests) {
		this.requests = requests;
	}

	public MailMessageRequest getCommons() {
		return commons;
	}

	public void setCommons(MailMessageRequest commons) {
		this.commons = commons;
	}
}
