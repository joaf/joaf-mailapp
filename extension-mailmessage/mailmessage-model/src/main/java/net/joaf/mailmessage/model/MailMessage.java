package net.joaf.mailmessage.model;

import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.db.StringUidEntity;
import net.joaf.mailmessage.model.enums.EMessageAction;
import net.joaf.mailmessage.model.enums.EMessageState;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Mail Message object
 *
 * @author Cyprian Śniegota
 * @since 1.0
 */
public class MailMessage implements Serializable, StringUidEntity {

	private String uid;
	private String subjectUid;
	private String templateUid;
	private String templateAlias;
	private EMessageState messageState;
	private MailMessageTemplate messageTemplate;
	private MailMessageRequest messageRequest;
	private MailMessageParsed messageParsed;
	private boolean parseProcessed = false;
	private LocalDateTime created;
	private LocalDateTime updated;
	private LocalDateTime parsed;
	private LocalDateTime sent;
	private String error;
	private EObjectState objectState;
	private Set<String> warnings = new HashSet<>();
	private String sourceIp;
	private List<EMessageAction> actions = new ArrayList<>();

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getSubjectUid() {
		return subjectUid;
	}

	public void setSubjectUid(String subjectUid) {
		this.subjectUid = subjectUid;
	}

	public String getTemplateUid() {
		return templateUid;
	}

	public void setTemplateUid(String templateUid) {
		this.templateUid = templateUid;
	}

	public String getTemplateAlias() {
		return templateAlias;
	}

	public void setTemplateAlias(String templateAlias) {
		this.templateAlias = templateAlias;
	}

	public EMessageState getMessageState() {
		return messageState;
	}

	public void setMessageState(EMessageState messageState) {
		this.messageState = messageState;
	}

	public MailMessageTemplate getMessageTemplate() {
		return messageTemplate;
	}

	public void setMessageTemplate(MailMessageTemplate messageTemplate) {
		this.messageTemplate = messageTemplate;
	}

	public MailMessageRequest getMessageRequest() {
		return messageRequest;
	}

	public void setMessageRequest(MailMessageRequest messageRequest) {
		this.messageRequest = messageRequest;
	}

	public MailMessageParsed getMessageParsed() {
		return messageParsed;
	}

	public void setMessageParsed(MailMessageParsed messageParsed) {
		this.messageParsed = messageParsed;
	}

	public boolean isParseProcessed() {
		return parseProcessed;
	}

	public void setParseProcessed(boolean parseProcessed) {
		this.parseProcessed = parseProcessed;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}

	public LocalDateTime getParsed() {
		return parsed;
	}

	public void setParsed(LocalDateTime parsed) {
		this.parsed = parsed;
	}

	public LocalDateTime getSent() {
		return sent;
	}

	public void setSent(LocalDateTime sent) {
		this.sent = sent;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Set<String> getWarnings() {
		return warnings;
	}

	public void setWarnings(Set<String> warnings) {
		this.warnings = warnings;
	}

	public String getSourceIp() {
		return sourceIp;
	}

	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}

	public EObjectState getObjectState() {
		return objectState;
	}

	public void setObjectState(EObjectState objectState) {
		this.objectState = objectState;
	}

	public List<EMessageAction> getActions() {
		return actions;
	}
}
