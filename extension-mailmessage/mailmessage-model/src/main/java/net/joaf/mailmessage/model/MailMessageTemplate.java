package net.joaf.mailmessage.model;

import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.db.StringUidEntity;
import net.joaf.mailmessage.model.enums.EMessageTemplateAction;
import net.joaf.mailmessage.model.enums.EMessageType;
import net.joaf.mailmessage.model.enums.EQueueState;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Mail Message template object
 *
 * @author Cyprian Śniegota
 * @since 1.0
 */
public class MailMessageTemplate implements Serializable, StringUidEntity {
	private String uid;
	private String alias;
	private String subjectUid;
	private String title;
	private String bodyHtml;
	private String bodyText;
	private String creditialsUid;
	private Map<String, String> headers = new HashMap<>();
	private Set<String> availableVars = new HashSet<>();
	private EObjectState objectState;
	private LocalDateTime created;
	private LocalDateTime updated;
	private Set<String> additionalTo = new HashSet<>();
	private Set<String> additionalCC = new HashSet<>();
	private Set<String> additionalBCC = new HashSet<>();
	private String from;
	private String replayTo;
	private EMessageType messageType;
	private EQueueState queueState;
	private List<EMessageTemplateAction> actions = new ArrayList<>();

	@Override
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getSubjectUid() {
		return subjectUid;
	}

	public void setSubjectUid(String subjectUid) {
		this.subjectUid = subjectUid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBodyHtml() {
		return bodyHtml;
	}

	public void setBodyHtml(String bodyHtml) {
		this.bodyHtml = bodyHtml;
	}

	public String getBodyText() {
		return bodyText;
	}

	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public Set<String> getAvailableVars() {
		return availableVars;
	}

	public void setAvailableVars(Set<String> availableVars) {
		this.availableVars = availableVars;
	}

	public EObjectState getObjectState() {
		return objectState;
	}

	public void setObjectState(EObjectState objectState) {
		this.objectState = objectState;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}

	public Set<String> getAdditionalTo() {
		return additionalTo;
	}

	public void setAdditionalTo(Set<String> additionalTo) {
		this.additionalTo = additionalTo;
	}

	public Set<String> getAdditionalCC() {
		return additionalCC;
	}

	public void setAdditionalCC(Set<String> additionalCC) {
		this.additionalCC = additionalCC;
	}

	public Set<String> getAdditionalBCC() {
		return additionalBCC;
	}

	public void setAdditionalBCC(Set<String> additionalBCC) {
		this.additionalBCC = additionalBCC;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getReplayTo() {
		return replayTo;
	}

	public void setReplayTo(String replayTo) {
		this.replayTo = replayTo;
	}

	public EMessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(EMessageType messageType) {
		this.messageType = messageType;
	}

	public EQueueState getQueueState() {
		return queueState;
	}

	public void setQueueState(EQueueState queueState) {
		this.queueState = queueState;
	}

	public List<EMessageTemplateAction> getActions() {
		return actions;
	}

	public String getCreditialsUid() {
		return creditialsUid;
	}

	public void setCreditialsUid(String creditialsUid) {
		this.creditialsUid = creditialsUid;
	}
}
