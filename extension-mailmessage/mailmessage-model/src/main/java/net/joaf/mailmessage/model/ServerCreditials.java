package net.joaf.mailmessage.model;

import net.joaf.base.core.db.StringUidEntity;
import net.joaf.base.core.db.entity.JoafSuidEntity;
import net.joaf.base.core.model.enums.EStandardAction;
import net.joaf.mailmessage.model.enums.EServerType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public class ServerCreditials extends JoafSuidEntity implements Serializable, StringUidEntity {
	private String name;
	private String host;
	private Integer port;
	private EServerType type;
	private String usernameandpassword;
	private transient String username;
	private transient String password;
	private List<EStandardAction> actions = new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public EServerType getType() {
		return type;
	}

	public void setType(EServerType type) {
		this.type = type;
	}

	public String getUsernameandpassword() {
		return usernameandpassword;
	}

	public void setUsernameandpassword(String usernameandpassword) {
		this.usernameandpassword = usernameandpassword;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<EStandardAction> getActions() {
		return actions;
	}

}
