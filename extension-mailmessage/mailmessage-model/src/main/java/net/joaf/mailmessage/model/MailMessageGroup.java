package net.joaf.mailmessage.model;

import net.joaf.base.core.db.StringUidEntity;

import java.io.Serializable;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public class MailMessageGroup implements StringUidEntity, Serializable{
	private String uid;
	private String subjectUid;
	private String templateAlias;
	private String code = "default";

	public String getSubjectUid() {
		return subjectUid;
	}

	public void setSubjectUid(String subjectUid) {
		this.subjectUid = subjectUid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTemplateAlias() {
		return templateAlias;
	}

	public void setTemplateAlias(String templateAlias) {
		this.templateAlias = templateAlias;
	}

	@Override
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
}
