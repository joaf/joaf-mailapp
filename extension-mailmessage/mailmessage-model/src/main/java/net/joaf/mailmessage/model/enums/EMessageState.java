package net.joaf.mailmessage.model.enums;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public enum EMessageState {
	CREATED, QUEUED, PARSED, SENT, ERROR
}
