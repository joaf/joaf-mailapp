package net.joaf.mailmessage.model.enums;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public enum EMessageType {
	HTML, TEXT, HTML_AND_TEXT;
}
