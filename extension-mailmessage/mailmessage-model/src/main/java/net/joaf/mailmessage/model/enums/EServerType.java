package net.joaf.mailmessage.model.enums;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public enum EServerType {
	MAIL_SMTP, MAIL_IMAP, MAIL_POP3
}
