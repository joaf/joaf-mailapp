package net.joaf.mailmessage.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Parsed template of mail message
 *
 * @author Cyprian Śniegota
 * @since 1.0
 */
public class MailMessageParsed implements Serializable {
	private String title;
	private String bodyHtml;
	private String bodyText;
	private Map<String, String> headers = new HashMap<>();
	private String from;
	private Set<String> to = new HashSet<>();
	private Set<String> cc = new HashSet<>();
	private Set<String> bcc = new HashSet<>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBodyHtml() {
		return bodyHtml;
	}

	public void setBodyHtml(String bodyHtml) {
		this.bodyHtml = bodyHtml;
	}

	public String getBodyText() {
		return bodyText;
	}

	public void setBodyText(String bodyText) {
		this.bodyText = bodyText;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public Set<String> getTo() {
		return to;
	}

	public void setTo(Set<String> to) {
		this.to = to;
	}

	public Set<String> getCc() {
		return cc;
	}

	public void setCc(Set<String> cc) {
		this.cc = cc;
	}

	public Set<String> getBcc() {
		return bcc;
	}

	public void setBcc(Set<String> bcc) {
		this.bcc = bcc;
	}
}
