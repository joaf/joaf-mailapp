package net.joaf.mailmessage.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Mail Message request for send
 *
 * @author Cyprian Śniegota
 * @since 1.0
 */
public class MailMessageRequest implements Serializable {

	private Map<String, String> model = new HashMap<>();
	private String messageAlias;
	private String subjectUid;
	private String from;
	private Set<String> to = new HashSet<>();
	private Set<String> cc = new HashSet<>();
	private Set<String> bcc = new HashSet<>();

	public Map<String, String> getModel() {
		return model;
	}

	public void setModel(Map<String, String> model) {
		this.model = model;
	}

	public String getMessageAlias() {
		return messageAlias;
	}

	public void setMessageAlias(String messageAlias) {
		this.messageAlias = messageAlias;
	}

	public String getSubjectUid() {
		return subjectUid;
	}

	public void setSubjectUid(String subjectUid) {
		this.subjectUid = subjectUid;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public Set<String> getTo() {
		return to;
	}

	public void setTo(Set<String> to) {
		this.to = to;
	}

	public Set<String> getCc() {
		return cc;
	}

	public void setCc(Set<String> cc) {
		this.cc = cc;
	}

	public Set<String> getBcc() {
		return bcc;
	}

	public void setBcc(Set<String> bcc) {
		this.bcc = bcc;
	}
}
