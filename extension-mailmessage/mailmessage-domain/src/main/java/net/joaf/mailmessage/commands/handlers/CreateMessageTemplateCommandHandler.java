package net.joaf.mailmessage.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailmessage.commands.CreateMessageTemplateCommand;
import net.joaf.mailmessage.model.MailMessageTemplate;
import net.joaf.mailmessage.model.enums.EMessageType;
import net.joaf.mailmessage.model.enums.EQueueState;
import net.joaf.mailmessage.repository.api.MailMessageTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;

/**
 * Created by cyprian on 14.03.15.
 */
@CommandHandlerComponent
public class CreateMessageTemplateCommandHandler implements CommandHandler<CreateMessageTemplateCommand, Object> {

	@Autowired
	private MailMessageTemplateRepository repository;

	@Override
	public BindingResult validate(BindingResult originalBindingResult, CreateMessageTemplateCommand command) throws JoafException {
		return originalBindingResult;
	}

	@Override
	public Object execute(CreateMessageTemplateCommand command) throws JoafException {
		MailMessageTemplate element = new MailMessageTemplate();
		element.setUid(repository.prepareId());
		element.setObjectState(EObjectState.NEW);
		LocalDateTime now = LocalDateTime.now();
		element.setCreated(now);
		element.setUpdated(now);
		element.setQueueState(EQueueState.UNAVAILABLE);
		element.setMessageType(EMessageType.HTML);
		element.setSubjectUid(command.getSubjectUid());
		repository.insert(element);
		return element;
	}
}