package net.joaf.mailmessage.parseengines;

import java.util.Map;

/**
 * Created by cyprian on 19.03.15.
 */
public interface TextParseEngine {

    String parse(String text, Map<String, String> model);

    ETextParseEngine engineName();

}
