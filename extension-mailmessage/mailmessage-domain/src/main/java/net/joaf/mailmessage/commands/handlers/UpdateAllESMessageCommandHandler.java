package net.joaf.mailmessage.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailmessage.commands.UpdateAllESMessageCommand;
import net.joaf.mailmessage.model.MailMessage;
import net.joaf.mailmessage.repository.api.MailMessageRepository;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@CommandHandlerComponent
public class UpdateAllESMessageCommandHandler implements CommandHandler<UpdateAllESMessageCommand, Object> {

	@Autowired
	private MailMessageRepository repository;

	@Override
	public BindingResult validate(BindingResult originalBindingResult, UpdateAllESMessageCommand command) throws JoafException {
		return originalBindingResult;
	}

	@Override
	public Object execute(UpdateAllESMessageCommand command) throws JoafException {
		Client client = new TransportClient().addTransportAddress(new InetSocketTransportAddress("localhost", 9300));
		List<MailMessage> all = repository.findAll();
		for (MailMessage mailMessage : all) {
			try {
				IndexResponse response = client.prepareIndex("messages", "message", mailMessage.getUid())
						.setSource(jsonBuilder()
										.startObject()
										.field("subjectUid", mailMessage.getSubjectUid())
										.field("alias", mailMessage.getTemplateAlias())
										.field("bodyHtml", mailMessage.getMessageParsed() != null? mailMessage.getMessageParsed().getBodyHtml():null)
										.endObject()
						)
						.execute()
						.actionGet();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		client.close();
		return null;
	}
}