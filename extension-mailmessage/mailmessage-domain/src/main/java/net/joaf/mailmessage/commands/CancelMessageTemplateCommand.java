package net.joaf.mailmessage.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.mailmessage.commands.handlers.CancelMessageTemplateCommandHandler;

@CommandDto(handlerClass = CancelMessageTemplateCommandHandler.class)
public class CancelMessageTemplateCommand extends AbstractUidSuidCommand implements Command {
	public CancelMessageTemplateCommand(String elementUid, String userUid, String subjectUid) {
		super(elementUid, userUid, subjectUid);
	}
}