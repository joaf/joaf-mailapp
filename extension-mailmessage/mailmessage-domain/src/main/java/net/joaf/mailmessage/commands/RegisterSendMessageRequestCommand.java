package net.joaf.mailmessage.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractCommand;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.mailmessage.commands.handlers.RegisterSendMessageRequestCommandHandler;
import net.joaf.mailmessage.model.MailMessageRequest;
import org.springframework.http.HttpRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@CommandDto(handlerClass = RegisterSendMessageRequestCommandHandler.class)
public class RegisterSendMessageRequestCommand extends AbstractCommand implements Command {

	private MailMessageRequest element;
	private String subjectUid;
	private String sourceIp;

	public RegisterSendMessageRequestCommand(MailMessageRequest element, String userUid, String subjectUid, HttpServletRequest request) {
		super(userUid, null);
		this.element = element;
		this.subjectUid = subjectUid;
		this.sourceIp = request.getRemoteUser();
	}

	public MailMessageRequest getElement() {
		return element;
	}

	public String getSubjectUid() {
		return subjectUid;
	}

	public String getSourceIp() {
		return sourceIp;
	}
}