package net.joaf.mailmessage.events;

import net.joaf.base.core.cqrs.Event;
import net.joaf.mailmessage.commands.RegisterSendMessageRequestCommand;
import net.joaf.mailmessage.model.MailMessage;

import java.io.Serializable;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public class SendMessageRequestRegisteredEvent implements Event, Serializable {

	private MailMessage command;
	private String aggregationUid;

	public SendMessageRequestRegisteredEvent(MailMessage command, String aggregationUid) {
		this.command = command;
		this.aggregationUid = aggregationUid;
	}

	public String getAggregationUid() {
		return aggregationUid;
	}

	public void setAggregationUid(String aggregationUid) {
		this.aggregationUid = aggregationUid;
	}

	public MailMessage getCommand() {
		return command;
	}

	public void setCommand(MailMessage command) {
		this.command = command;
	}
}