package net.joaf.mailmessage;

import net.joaf.base.extension.AbstractExtension;
import net.joaf.base.extension.JoafExtension;
import org.springframework.stereotype.Component;

/**
 * Created by cyprian on 15.03.15.
 */
@Component
public class MailMessageExtension extends AbstractExtension implements JoafExtension {
    public static final String EXTENSION_BASE_PATH = "/";
    public static final String EXTENSION_ADMIN_BASE_PATH = "/administration/mailmessage";

    @Override
    public String getExtensionMetadataFile() {
        return "/extension-mailmessage.xml";
    }
}