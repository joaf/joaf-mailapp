package net.joaf.mailmessage.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.mailmessage.commands.handlers.UndoTrashMessageTemplateCommandHandler;

/**
 * Created by cyprian on 14.03.15.
 */
@CommandDto(handlerClass = UndoTrashMessageTemplateCommandHandler.class)
public class UndoTrashMessageTemplateCommand extends AbstractUidSuidCommand implements Command {
	public UndoTrashMessageTemplateCommand(String elementUid, String userUid, String subjectUid) {
		super(elementUid, userUid, subjectUid);
	}
}