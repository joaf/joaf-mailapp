package net.joaf.mailmessage.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.mailmessage.commands.handlers.TrashMessageTemplateCommandHandler;

/**
 * Created by cyprian on 14.03.15.
 */
@CommandDto(handlerClass = TrashMessageTemplateCommandHandler.class)
public class TrashMessageTemplateCommand extends AbstractUidSuidCommand implements Command {
	public TrashMessageTemplateCommand(String elementUid, String userUid, String subjectUid) {
		super(elementUid, userUid, subjectUid);
	}
}