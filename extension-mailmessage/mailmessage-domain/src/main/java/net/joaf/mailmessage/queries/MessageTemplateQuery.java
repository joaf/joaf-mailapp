package net.joaf.mailmessage.queries;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mailmessage.model.MailMessageTemplate;
import net.joaf.mailmessage.model.enums.EMessageTemplateAction;
import net.joaf.mailmessage.repository.api.MailMessageTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by cyprian on 15.03.15.
 */
@Service
public class MessageTemplateQuery {
    @Autowired
    private MailMessageTemplateRepository repository;

    public PageredResult<MailMessageTemplate> findAll(PageredRequest request) throws JoafDatabaseException {
        PageredResult<MailMessageTemplate> all = repository.findAll(request);
        all.getResult().forEach(this::appendActions);
        return all;
    }

    public MailMessageTemplate findOne(String uid) throws JoafDatabaseException {
        return repository.findOne(uid);
    }

    private void appendActions(MailMessageTemplate item) {
        item.getActions().add(EMessageTemplateAction.EDIT);
        item.getActions().add(EMessageTemplateAction.TRASH);
		item.getActions().add(EMessageTemplateAction.PREPARE_SEND);
    }
}
