package net.joaf.mailmessage.parseengines;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by cyprian on 19.03.15.
 */
@Component
public class TextParseEngineFactory {
    @Autowired
    private List<TextParseEngine> engines;

    public TextParseEngine getEngine(ETextParseEngine engineName) {
        for (TextParseEngine engine : engines) {
            if (engineName.equals(engine.engineName())) {
                return engine;
            }
        }
        return null;
    }


}
