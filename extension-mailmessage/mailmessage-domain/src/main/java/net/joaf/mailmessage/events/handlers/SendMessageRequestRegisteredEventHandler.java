package net.joaf.mailmessage.events.handlers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.Event;
import net.joaf.base.core.cqrs.EventHandler;
import net.joaf.base.core.cqrs.EventPublisher;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailmessage.commands.ChangeMessageStateCommand;
import net.joaf.mailmessage.commands.handlers.ChangeMessageStateCommandHandler;
import net.joaf.mailmessage.events.SendMessageRequestRegisteredEvent;
import net.joaf.mailmessage.model.enums.EMessageState;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Component
public class SendMessageRequestRegisteredEventHandler implements EventHandler {

	@Autowired
	private EventPublisher eventPublisher;

	@Autowired
	private CommandGateway commandGateway;

	@Produce(uri = "direct:enqueueMessageRequest")
	private ProducerTemplate template = null;

	@PostConstruct
	public void setUp() {
		eventPublisher.register(SendMessageRequestRegisteredEventHandler.class, SendMessageRequestRegisteredEvent.class);
	}

	@Override
	public void execute(Event event) throws JoafException {
		if (template != null) {
			SendMessageRequestRegisteredEvent customEvent = (SendMessageRequestRegisteredEvent) event;
			try {
				commandGateway.send(new ChangeMessageStateCommand(customEvent.getCommand().getUid(), EMessageState.QUEUED, "", customEvent.getAggregationUid()),
						ChangeMessageStateCommandHandler.class);
				template.sendBodyAndHeader(customEvent.getCommand(), "aggregateUid", customEvent.getAggregationUid());
			} catch (Exception e) {
				commandGateway
						.send(new ChangeMessageStateCommand(customEvent.getCommand().getUid(), EMessageState.CREATED, "", customEvent.getAggregationUid()),
								ChangeMessageStateCommandHandler.class);
				throw e;
			}
		}
	}
}