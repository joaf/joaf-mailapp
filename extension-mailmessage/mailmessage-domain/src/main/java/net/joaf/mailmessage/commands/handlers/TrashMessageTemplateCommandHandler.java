package net.joaf.mailmessage.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailmessage.commands.TrashMessageTemplateCommand;
import net.joaf.mailmessage.repository.api.MailMessageTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 14.03.15.
 */
@CommandHandlerComponent
public class TrashMessageTemplateCommandHandler implements CommandHandler<TrashMessageTemplateCommand, Object> {

	@Autowired
	private MailMessageTemplateRepository repository;

	@Override
	public BindingResult validate(BindingResult originalBindingResult, TrashMessageTemplateCommand command) throws JoafException {
		return originalBindingResult;
	}

	@Override
	public Object execute(TrashMessageTemplateCommand command) throws JoafException {
		repository.updateObjectState(command.getElementUid(), EObjectState.TRASH);
		return null;
	}
}