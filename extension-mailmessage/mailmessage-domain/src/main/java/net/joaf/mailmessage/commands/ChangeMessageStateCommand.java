package net.joaf.mailmessage.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractCommand;
import net.joaf.mailmessage.commands.handlers.ChangeMessageStateCommandHandler;
import net.joaf.mailmessage.model.enums.EMessageState;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@CommandDto(handlerClass = ChangeMessageStateCommandHandler.class)
public class ChangeMessageStateCommand extends AbstractCommand implements Command {

	private EMessageState newState;
	private String uid;

	public ChangeMessageStateCommand(String uid, EMessageState newState, String userUid, String aggregateUid) {
		super(userUid, aggregateUid);
		this.uid = uid;
		this.newState = newState;
	}

	public String getUid() {
		return uid;
	}

	public EMessageState getNewState() {
		return newState;
	}
}