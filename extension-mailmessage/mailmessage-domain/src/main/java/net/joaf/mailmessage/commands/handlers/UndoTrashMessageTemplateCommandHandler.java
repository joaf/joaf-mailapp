package net.joaf.mailmessage.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailmessage.commands.UndoTrashMessageTemplateCommand;
import net.joaf.mailmessage.repository.api.MailMessageTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 14.03.15.
 */
@CommandHandlerComponent
public class UndoTrashMessageTemplateCommandHandler implements CommandHandler<UndoTrashMessageTemplateCommand, Object> {

	@Autowired
	private MailMessageTemplateRepository repository;

	@Override
	public BindingResult validate(BindingResult originalBindingResult, UndoTrashMessageTemplateCommand command) throws JoafException {
		return originalBindingResult;
	}

	@Override
	public Object execute(UndoTrashMessageTemplateCommand command) throws JoafException {
		repository.updateObjectState(command.getElementUid(), EObjectState.ACCEPTED);
		return null;
	}
}