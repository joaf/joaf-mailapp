package net.joaf.mailmessage.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailmessage.commands.ChangeMessageStateCommand;
import net.joaf.mailmessage.repository.api.MailMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@CommandHandlerComponent
public class ChangeMessageStateCommandHandler implements CommandHandler<ChangeMessageStateCommand, Object> {

	@Autowired
	private MailMessageRepository repository;

	@Override
	public BindingResult validate(BindingResult originalBindingResult, ChangeMessageStateCommand command) throws JoafException {
		return originalBindingResult;
	}

	@Override
	public Object execute(ChangeMessageStateCommand command) throws JoafException {
		repository.updateMessageState(command.getUid(), command.getNewState());
		return null;
	}
}