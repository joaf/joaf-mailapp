package net.joaf.mailmessage.queries;

import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mailmessage.model.MailMessage;
import net.joaf.mailmessage.model.enums.EMessageAction;
import net.joaf.mailmessage.repository.api.MailMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by cyprian on 18.03.15.
 */
@Service
public class MessageQuery {

    @Autowired
    private MailMessageRepository repository;

    public PageredResult<MailMessage> list(PageredRequest request) throws JoafDatabaseException {
		PageredResult<MailMessage> all = repository.findAll(request);
		all.getElements().stream().forEach(x -> x.getData().getActions().add(EMessageAction.RESEND));
		return all;
    }
}
