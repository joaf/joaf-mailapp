package net.joaf.mailmessage.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractCommand;
import net.joaf.mailmessage.commands.handlers.UpdateAllESMessageCommandHandler;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@CommandDto(handlerClass = UpdateAllESMessageCommandHandler.class)
public class UpdateAllESMessageCommand extends AbstractCommand implements Command {
	public UpdateAllESMessageCommand(String userUid) {
		super(userUid);
	}

}
