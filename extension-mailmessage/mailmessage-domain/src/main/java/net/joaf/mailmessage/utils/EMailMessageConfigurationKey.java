package net.joaf.mailmessage.utils;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public enum EMailMessageConfigurationKey {
	SERVER_MAXINSTANCES("mailmessage.server.maxinstances.number", "1")
	,TEMPLATE_MAXINSTANCES("mailmessage.template.maxinstances.number", "1")
	,QUEUE_PERMINUTE("mailmessage.queue.perminute.number", "5")
	;
	private String key;
	private String defValue;

	EMailMessageConfigurationKey(String key, String defValue) {
		this.key = key;
		this.defValue = defValue;
	}

	public String getKey() {
		return key;
	}

	public String getDefValue() {
		return defValue;
	}
}
