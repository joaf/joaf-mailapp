package net.joaf.mailmessage.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailmessage.commands.CancelMessageTemplateCommand;
import net.joaf.mailmessage.model.MailMessageTemplate;
import net.joaf.mailmessage.repository.api.MailMessageTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

/**
 * Created by cyprian on 14.03.15.
 */
@CommandHandlerComponent
public class CancelMessageTemplateCommandHandler implements CommandHandler<CancelMessageTemplateCommand, Object> {

	@Autowired
	private MailMessageTemplateRepository repository;

	@Override
	public BindingResult validate(BindingResult originalBindingResult, CancelMessageTemplateCommand command) throws JoafException {
		return originalBindingResult;
	}

	@Override
	public Object execute(CancelMessageTemplateCommand command) throws JoafException {
		MailMessageTemplate element = repository.findOne(command.getElementUid());
		if (EObjectState.NEW.equals(element.getObjectState())) {
			repository.remove(command.getElementUid());
		}
		return null;
	}
}