package net.joaf.mailmessage.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailmessage.commands.SaveMessageTemplateCommand;
import net.joaf.mailmessage.model.MailMessageTemplate;
import net.joaf.mailmessage.repository.api.MailMessageTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;

/**
 * Created by cyprian on 14.03.15.
 */
@CommandHandlerComponent
public class SaveMessageTemplateCommandHandler implements CommandHandler<SaveMessageTemplateCommand, Object> {

	@Autowired
	private MailMessageTemplateRepository repository;

	@Override
	public BindingResult validate(BindingResult originalBindingResult, SaveMessageTemplateCommand command) throws JoafException {
		return originalBindingResult;
	}

	@Override
	public Object execute(SaveMessageTemplateCommand command) throws JoafException {
		MailMessageTemplate dbObject = repository.findOne(command.getElement().getUid());
		if (EObjectState.NEW.equals(dbObject.getObjectState())) {
			repository.updateObjectState(command.getElement().getUid(), EObjectState.ACCEPTED);
		}
		command.getElement().setUpdated(LocalDateTime.now());
		repository.store(command.getElement());
		return null;
	}
}