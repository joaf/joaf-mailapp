package net.joaf.mailmessage.camel;

import com.sun.mail.smtp.SMTPTransport;
import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafException;
import net.joaf.connection.smtp.model.SmtpConnectionMetadata;
import net.joaf.mailmessage.commands.ChangeMessageStateCommand;
import net.joaf.mailmessage.commands.handlers.ChangeMessageStateCommandHandler;
import net.joaf.mailmessage.model.MailMessage;
import net.joaf.mailmessage.model.MailMessageParsed;
import net.joaf.mailmessage.model.enums.EMessageState;
import net.joaf.mailmessage.parseengines.ETextParseEngine;
import net.joaf.mailmessage.parseengines.TextParseEngine;
import net.joaf.mailmessage.parseengines.TextParseEngineFactory;
import net.joaf.mailmessage.repository.api.MailMessageRepository;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import net.joaf.mainextensions.connection.repository.api.ExternalConnectionRepository;
import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Component
public class ProcessMessageRequestBean {

	@Autowired
	private TextParseEngineFactory textParseEngineFactory;

	@Autowired
	private MailMessageRepository repository;

	@Autowired
	private ExternalConnectionRepository externalConnectionRepository;

	@Autowired
	private CommandGateway commandGateway;

	@Handler
	public void endpoint(MailMessage body, Exchange exchange) {
		try {
			//parse
			TextParseEngine engine = textParseEngineFactory.getEngine(ETextParseEngine.REPLACE);
			MailMessageParsed parsed = new MailMessageParsed();
			Map<String, String> model = body.getMessageRequest().getModel();
			parsed.setBodyHtml(engine.parse(body.getMessageTemplate().getBodyHtml(), model));
			parsed.setBodyText(engine.parse(body.getMessageTemplate().getBodyText(), model));
			parsed.setTitle(engine.parse(body.getMessageTemplate().getTitle(), model));
			Map<String, String> headers = new HashMap<>();
			for (String headerKey : body.getMessageTemplate().getHeaders().keySet()) {
				String parsedKey = engine.parse(headerKey, model);
				String parsedValue = engine.parse(body.getMessageTemplate().getHeaders().get(headerKey), model);
				headers.put(parsedKey, parsedValue);
			}
			parsed.getTo().addAll(body.getMessageTemplate().getAdditionalTo());
			parsed.getTo().addAll(body.getMessageRequest().getTo());
			parsed.getCc().addAll(body.getMessageTemplate().getAdditionalCC());
			parsed.getBcc().addAll(body.getMessageTemplate().getAdditionalBCC());
			parsed.setFrom(body.getMessageTemplate().getFrom());

			parsed.getHeaders().putAll(headers);
			body.setMessageParsed(parsed);
			repository.updateParsed(body.getUid(), parsed);
			//send
			this.sendEmail(body);
			commandGateway.send(new ChangeMessageStateCommand(body.getUid(), EMessageState.SENT, "", null), ChangeMessageStateCommandHandler.class);
		} catch (JoafException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			try {
				repository.updateError(body.getUid(), e.getLocalizedMessage());
			} catch (JoafDatabaseException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
	}

	private void sendEmail(MailMessage body) throws MessagingException, JoafDatabaseException {
		ExternalConnection one = externalConnectionRepository.findOne(body.getMessageTemplate().getCreditialsUid());
		SmtpConnectionMetadata smtpConnectionMetadata = (SmtpConnectionMetadata) one.getConnectionMetadata();
		String[] usernameandpassword = smtpConnectionMetadata.getUsernameandpassword().split(":");
		smtpConnectionMetadata.setUsername(usernameandpassword[0]);
		smtpConnectionMetadata.setPassword(usernameandpassword[1]);
		MailMessageParsed parsed = body.getMessageParsed();
		Properties properties = System.getProperties();

		Session session = Session.getDefaultInstance(properties);
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(parsed.getFrom()));
		for (String to : parsed.getTo()) {
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		}
		message.setSubject(parsed.getTitle(), "UTF-8");
		message.setContent(parsed.getBodyHtml(), "text/html;charset=UTF-8");
		//		message.setText(parsed.getBodyText() == null ? "" : parsed.getBodyText());
		URLName urlName = new URLName("smtp", smtpConnectionMetadata.getHost(), smtpConnectionMetadata.getPort(), null, smtpConnectionMetadata.getUsername(),
				smtpConnectionMetadata.getPassword());
		SMTPTransport transport = (SMTPTransport) session.getTransport(urlName);
		transport.connect();
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();
	}
}
