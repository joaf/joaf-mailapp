package net.joaf.mailmessage.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractEntityCommand;
import net.joaf.mailmessage.commands.handlers.SaveMessageTemplateCommandHandler;
import net.joaf.mailmessage.model.MailMessageTemplate;

/**
 * Created by cyprian on 14.03.15.
 */
@CommandDto(handlerClass = SaveMessageTemplateCommandHandler.class)
public class SaveMessageTemplateCommand extends AbstractEntityCommand<MailMessageTemplate> implements Command {
	public SaveMessageTemplateCommand(MailMessageTemplate element, String userUid) {
		super(element, userUid);
	}
}