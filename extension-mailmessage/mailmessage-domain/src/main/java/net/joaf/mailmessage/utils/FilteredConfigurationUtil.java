package net.joaf.mailmessage.utils;

import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.mainextensions.configuration.helpers.ConfigurationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Component
public class FilteredConfigurationUtil {

	private static final Map<String, String> defaults = new HashMap<>();
	private static final String MODULE = "mailmessage";

	@Autowired
	private ConfigurationHelper configurationHelper;
	static {
		for (EMailMessageConfigurationKey eMailMessageConfigurationKey : EMailMessageConfigurationKey.values()) {
			defaults.put(eMailMessageConfigurationKey.getKey(), eMailMessageConfigurationKey.getDefValue());
		}
	}

	public String getValue(String userUid, String subjectUid, String key) throws JoafDatabaseException {
		if (defaults.containsKey(key)) {
			return defaults.get(key);
		} else {
			return configurationHelper.getValue(MODULE, key);
		}
	}

	public Integer getValueInteger(String userUid, String subjectUid, String key) throws JoafDatabaseException {
		String val = this.getValue(userUid, subjectUid, key);
		return val == null ? 0 : Integer.valueOf(val);
	}

}
