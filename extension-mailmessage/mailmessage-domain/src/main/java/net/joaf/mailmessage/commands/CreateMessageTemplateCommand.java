package net.joaf.mailmessage.commands;

import net.joaf.base.core.cqrs.Command;
import net.joaf.base.core.cqrs.annotations.CommandDto;
import net.joaf.base.core.cqrs.commands.AbstractUidSuidCommand;
import net.joaf.mailmessage.commands.handlers.CreateMessageTemplateCommandHandler;

/**
 * Created by cyprian on 14.03.15.
 */
@CommandDto(handlerClass = CreateMessageTemplateCommandHandler.class)
public class CreateMessageTemplateCommand extends AbstractUidSuidCommand implements Command {
	public CreateMessageTemplateCommand(String userUid, String subjectUid) {
		super(null, userUid, subjectUid);
	}
}