package net.joaf.mailmessage.parseengines;

import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by cyprian on 19.03.15.
 */
@Component
public class ReplaceTextParseEngine implements TextParseEngine {

    @Override
    public String parse(String text, Map<String, String> model) {
        if (text == null) {
            return null;
        }
        String parsed = text;
        for (String key : model.keySet()) {
            parsed = parsed.replaceAll(key(key), model.get(key));
        }
        return parsed;
    }

    private String key(String name) {
        return "\\{" + name + "\\}";
    }

    @Override
    public ETextParseEngine engineName() {
        return ETextParseEngine.REPLACE;
    }

}
