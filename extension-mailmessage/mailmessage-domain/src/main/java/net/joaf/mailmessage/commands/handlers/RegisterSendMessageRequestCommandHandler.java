package net.joaf.mailmessage.commands.handlers;

import net.joaf.base.core.cqrs.CommandHandler;
import net.joaf.base.core.cqrs.EventPublisher;
import net.joaf.base.core.cqrs.annotations.CommandHandlerComponent;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafException;
import net.joaf.mailmessage.commands.RegisterSendMessageRequestCommand;
import net.joaf.mailmessage.events.SendMessageRequestRegisteredEvent;
import net.joaf.mailmessage.model.MailMessage;
import net.joaf.mailmessage.model.MailMessageTemplate;
import net.joaf.mailmessage.model.enums.EMessageState;
import net.joaf.mailmessage.repository.api.MailMessageRepository;
import net.joaf.mailmessage.repository.api.MailMessageTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@CommandHandlerComponent
public class RegisterSendMessageRequestCommandHandler implements CommandHandler<RegisterSendMessageRequestCommand, Object> {

	@Autowired
	private MailMessageTemplateRepository templateRepository;
	@Autowired
	private MailMessageRepository repository;
	@Autowired
	private EventPublisher eventPublisher;

	@Override
	public BindingResult validate(BindingResult originalBindingResult, RegisterSendMessageRequestCommand command) throws JoafException {
		return originalBindingResult;
	}

	@Override
	public Object execute(RegisterSendMessageRequestCommand command) throws JoafException {
		MailMessage element = new MailMessage();
		element.setMessageRequest(command.getElement());
		MailMessageTemplate template = templateRepository.findByAlias(command.getSubjectUid(), command.getElement().getMessageAlias());
		element.setMessageTemplate(template);
		element.setError("");
		element.setCreated(LocalDateTime.now());
		element.setUpdated(LocalDateTime.now());
		element.setMessageState(EMessageState.CREATED);
		element.setObjectState(EObjectState.ACCEPTED);
		element.setSourceIp(command.getSourceIp());
		element.setSubjectUid(command.getSubjectUid());
		element.setTemplateAlias(template.getAlias());
		element.setTemplateUid(template.getUid());
		element.setUid(repository.prepareId());
		repository.insert(element);
		eventPublisher.post(new SendMessageRequestRegisteredEvent(element, command.getAggregateId()));
		return null;
	}
}