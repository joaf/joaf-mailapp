package net.joaf.mailmessage.repository.mysql;

import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.db.NoSqlRepository;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.base.core.utils.JsonUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mailmessage.model.MailMessageTemplate;
import net.joaf.mailmessage.model.enums.EMessageType;
import net.joaf.mailmessage.model.enums.EQueueState;
import net.joaf.mailmessage.repository.api.MailMessageTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Created by cyprian on 15.03.15.
 */
@Repository
public class MailMessageTemplateRepositoryMysql extends AbstractRepositoryMysql<MailMessageTemplate> implements NoSqlRepository, MailMessageTemplateRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	protected String getTableName() {
		return "mailmessagetemplate";
	}

	@Override
	protected JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	protected RowMapper<MailMessageTemplate> getRowMapper() {
		return (resultSet, i) -> {
			MailMessageTemplate element = new MailMessageTemplate();
			element.setUid(resultSet.getString("uid"));
			element.setCreated(DateUtils.dateToLocalDateTime(resultSet.getDate("created")));
			element.setUpdated(DateUtils.dateToLocalDateTime(resultSet.getDate("updated")));
			element.setObjectState(EObjectState.valueOf(resultSet.getString("objectState")));
			element.setSubjectUid(resultSet.getString("subjectUid"));
			element.setAlias(resultSet.getString("alias"));
			element.setBodyHtml(resultSet.getString("bodyHtml"));
			element.setBodyText(resultSet.getString("bodyText"));
			element.setFrom(resultSet.getString("from"));
			element.setTitle(resultSet.getString("title"));
			element.setCreditialsUid(resultSet.getString("creditialsUid"));
			element.setMessageType(EMessageType.valueOf(resultSet.getString("messageType")));
			element.setQueueState(EQueueState.valueOf(resultSet.getString("queueState")));
			element.setAdditionalBCC(JsonUtils.getPojoSet(resultSet.getString("additionalBcc"), String.class));
			element.setAdditionalCC(JsonUtils.getPojoSet(resultSet.getString("additionalCc"), String.class));
			element.setAdditionalTo(JsonUtils.getPojoSet(resultSet.getString("additionalTo"), String.class));
			element.setAvailableVars(JsonUtils.getPojoSet(resultSet.getString("availableVars"), String.class));
            String headers = resultSet.getString("headers");
            element.setHeaders(JsonUtils.getPojoStringMap(headers, String.class));
			element.setReplayTo(resultSet.getString("replayTO"));
			return element;
		};
	}

	@Override
	public void insert(MailMessageTemplate element) throws JoafDatabaseException {
		try {
			String sql = "INSERT INTO " + getTableName()
					+ " (uid, created, updated, objectState, subjectUid, alias, bodyHtml, bodyText, `from`, title, messageType, queueState" +
					", additionalBcc, additionalCc, additionalTo, availableVars, headers, replayTo, creditialsUid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			jdbcTemplate.update(sql, element.getUid(), DateUtils.localDateTimeToDate(element.getCreated()), DateUtils.localDateTimeToDate(element.getUpdated())
					, element.getObjectState().toString()
					, element.getSubjectUid(), element.getAlias(), element.getBodyHtml(), element.getBodyText(), element.getFrom(), element.getTitle()
					, element.getMessageType().toString(), element.getQueueState().toString(), JsonUtils.stringValue(element.getAdditionalBCC())
					, JsonUtils.stringValue(element.getAdditionalCC()), JsonUtils.stringValue(element.getAdditionalTo())
					, JsonUtils.stringValue(element.getAvailableVars()), JsonUtils.stringValue(element.getHeaders()), element.getCreditialsUid()
					, element.getReplayTo());

		} catch (Exception e) {
			throw new JoafDatabaseException("Repository error", e);
		}
	}

	@Override
	public void store(MailMessageTemplate element) throws JoafDatabaseException {
		try {
			String sql = "UPDATE " + getTableName() + " SET updated=?, alias=?, bodyHtml=?, bodyText=?, `from`=?, title=?" +
					", additionalBcc=?, additionalCc=?, additionalTo=?, availableVars=?, headers=?, replayTo=?, creditialsUid=? where uid=?";
			jdbcTemplate.update(sql, DateUtils.localDateTimeToDate(element.getUpdated()), element.getAlias(), element.getBodyHtml(), element.getBodyText()
					, element.getFrom(), element.getTitle()
					//, element.getMessageType().toString()
					, JsonUtils.stringValue(element.getAdditionalBCC())
					, JsonUtils.stringValue(element.getAdditionalCC()), JsonUtils.stringValue(element.getAdditionalTo())
					, JsonUtils.stringValue(element.getAvailableVars()), JsonUtils.stringValue(element.getHeaders())
					, element.getReplayTo(), element.getCreditialsUid()
					, element.getUid());
		} catch (Exception e) {
			throw new JoafDatabaseException("Repository error", e);
		}
	}

	public void updateState(String uid, EObjectState newState) throws JoafDatabaseException {
		try {
			String sql = "UPDATE " + getTableName() + " SET objectState=? where uid=?";
			jdbcTemplate.update(sql, newState.toString(), uid);
		} catch (Exception e) {
			throw new JoafDatabaseException("Repository error", e);
		}
	}

	@Override
	public String collectionName() {
		return getTableName();
	}

	@Override
	public Class<MailMessageTemplate> getEntityClass() {
		return MailMessageTemplate.class;
	}

	@Override
	public String prepareId() {
		return UUID.randomUUID().toString();
	}

	@Override
	public MailMessageTemplate findByAlias(String subjectUid, String messageAlias) throws JoafDatabaseException {
		try {
			String sql = "SELECT * FROM {tableName} WHERE alias = ? and subjectUid = ?";
			return getJdbcTemplate().query(tableQuery(sql), new Object[]{messageAlias, subjectUid}, this.getRowMapper()).stream().findFirst().get();
		} catch (Exception e) {
			throw createDatabaseException(e);
		}
	}
}
