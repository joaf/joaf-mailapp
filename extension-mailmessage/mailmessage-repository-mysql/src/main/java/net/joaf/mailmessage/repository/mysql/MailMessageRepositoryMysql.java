package net.joaf.mailmessage.repository.mysql;

import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.utils.DateUtils;
import net.joaf.base.core.utils.JsonUtils;
import net.joaf.base.db.mysql.AbstractRepositoryMysql;
import net.joaf.mailmessage.model.MailMessage;
import net.joaf.mailmessage.model.MailMessageParsed;
import net.joaf.mailmessage.model.MailMessageRequest;
import net.joaf.mailmessage.model.MailMessageTemplate;
import net.joaf.mailmessage.model.enums.EMessageState;
import net.joaf.mailmessage.repository.api.MailMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Repository
public class MailMessageRepositoryMysql extends AbstractRepositoryMysql<MailMessage> implements MailMessageRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	protected String getTableName() {
		return "mailmessage";
	}

	@Override
	protected JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	@Override
	protected RowMapper<MailMessage> getRowMapper() {
		return (resultSet, i) -> {
			MailMessage element = new MailMessage();
			element.setUid(resultSet.getString("uid"));
			element.setCreated(DateUtils.dateToLocalDateTime(resultSet.getDate("created")));
			element.setUpdated(DateUtils.dateToLocalDateTime(resultSet.getDate("updated")));
			element.setObjectState(EObjectState.valueOf(resultSet.getString("objectState")));
			element.setSubjectUid(resultSet.getString("subjectUid"));
			//
			element.setTemplateUid(resultSet.getString("templateUid"));
			element.setTemplateAlias(resultSet.getString("templateAlias"));
			element.setMessageState(EMessageState.valueOf(resultSet.getString("messageState")));
			element.setSourceIp(resultSet.getString("sourceIp"));
			element.setParseProcessed(resultSet.getBoolean("parseProcessed"));
			element.setParsed(DateUtils.dateToLocalDateTime(resultSet.getDate("parsed")));
			element.setSent(DateUtils.dateToLocalDateTime(resultSet.getDate("sent")));
			element.setError(resultSet.getString("error"));
			element.setWarnings(JsonUtils.getPojoSet(resultSet.getString("warnings"), String.class));
			element.setMessageTemplate(JsonUtils.getPojo(resultSet.getString("messageTemplate"), MailMessageTemplate.class));
			element.setMessageRequest(JsonUtils.getPojo(resultSet.getString("messageRequest"), MailMessageRequest.class));
			element.setMessageParsed(JsonUtils.getPojo(resultSet.getString("messageParsed"), MailMessageParsed.class));
			return element;
		};
	}

	@Override
	public void insert(MailMessage element) throws JoafDatabaseException {
		try {
			String sql = "INSERT INTO " + getTableName()
					+ " (uid, created, updated, objectState, subjectUid, templateUid, templateAlias, messageState, sourceIp, parseProcessed, parsed, sent" +
					", error, warnings, messageTemplate, messageRequest, messageParsed) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			jdbcTemplate.update(sql, element.getUid(), DateUtils.localDateTimeToDate(element.getCreated()), DateUtils.localDateTimeToDate(element.getUpdated())
					, element.getObjectState().toString()
					, element.getSubjectUid(), element.getTemplateUid(), element.getTemplateAlias(), element.getMessageState().toString(),
					element.getSourceIp(), element.getMessageParsed()
					, DateUtils.localDateTimeToDate(element.getParsed()), DateUtils.localDateTimeToDate(element.getSent()), element.getError()
					, JsonUtils.stringValue(element.getWarnings()), JsonUtils.stringValue(element.getMessageTemplate())
					, JsonUtils.stringValue(element.getMessageRequest()), JsonUtils.stringValue(element.getMessageParsed())
			);

		} catch (Exception e) {
			throw new JoafDatabaseException("Repository error", e);
		}
	}

	@Override
	public void store(MailMessage element) throws JoafDatabaseException {
		try {
			String sql = "UPDATE " + getTableName()
					+ " SET updated=?, templateUid=?, templateAlias=?, messageState=?, sourceIp=?, parseProcessed=?, parsed=?, sent=?" +
					", error=?, warnings=?, messageTemplate=?, messageRequest=?, messageParsed=? where uid=?";
			jdbcTemplate.update(sql, DateUtils.localDateTimeToDate(element.getUpdated())
					, element.getTemplateUid(), element.getTemplateAlias(), element.getMessageState().toString(), element.getSourceIp(),
					element.getMessageParsed()
					, DateUtils.localDateTimeToDate(element.getParsed()), DateUtils.localDateTimeToDate(element.getSent()), element.getError()
					, JsonUtils.stringValue(element.getWarnings()), JsonUtils.stringValue(element.getMessageTemplate())
					, JsonUtils.stringValue(element.getMessageRequest()), JsonUtils.stringValue(element.getMessageParsed())
					, element.getUid());
		} catch (Exception e) {
			throw new JoafDatabaseException("Repository error", e);
		}
	}

	public void updateState(String uid, EObjectState newState) throws JoafDatabaseException {
		try {
			String sql = "UPDATE " + getTableName() + " SET objectState=? where uid=?";
			jdbcTemplate.update(sql, newState.toString(), uid);
		} catch (Exception e) {
			throw new JoafDatabaseException("Repository error", e);
		}
	}

	@Override
	public String collectionName() {
		return getTableName();
	}

	@Override
	public Class<MailMessage> getEntityClass() {
		return MailMessage.class;
	}

	@Override
	public String prepareId() {
		return UUID.randomUUID().toString();
	}

	@Override
	public void updateMessageState(String uid, EMessageState newState) throws JoafDatabaseException {
		try {
			String sql = "UPDATE " + getTableName() + " SET messageState=? where uid=?";
			jdbcTemplate.update(sql, newState.toString(), uid);
		} catch (Exception e) {
			throw new JoafDatabaseException("Repository error", e);
		}
	}

	@Override
	public void updateParsed(String uid, MailMessageParsed parsed) throws JoafDatabaseException {
		try {
			String sql = "UPDATE " + getTableName() + " SET messageParsed=?, parsed=?, updated=? where uid=?";
			Date now = DateUtils.localDateTimeToDate(LocalDateTime.now());
			jdbcTemplate.update(sql, JsonUtils.stringValue(parsed), now, now, uid);
		} catch (Exception e) {
			throw new JoafDatabaseException("Repository error", e);
		}
	}

	@Override
	public void updateError(String uid, String localizedMessage) throws JoafDatabaseException {
		try {
			String sql = "UPDATE " + getTableName() + " SET error=?, messageState=?, updated=? where uid=?";
			Date now = DateUtils.localDateTimeToDate(LocalDateTime.now());
			jdbcTemplate.update(sql, localizedMessage, EMessageState.ERROR.toString(), now, uid);
		} catch (Exception e) {
			throw new JoafDatabaseException("Repository error", e);
		}

	}
}
