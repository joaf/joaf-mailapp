package net.joaf.mailmessage.parseengines;

import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.*;

public class ReplaceTextParseEngineTest {

    @Test
    public void testParse() throws Exception {
        //given
        ReplaceTextParseEngine objectUnderTest = new ReplaceTextParseEngine();
        String text = "Super text {text} replaced";
        Map<String, String> model = new HashMap<>();
        model.put("text", "should be");
        //when
        String parsed = objectUnderTest.parse(text, model);
        //then
        assertEquals(parsed, "Super text should be replaced");
    }
}