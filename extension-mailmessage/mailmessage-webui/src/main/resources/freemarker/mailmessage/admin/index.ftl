<#import "/templates/base.ftl" as page />
<@page.layout "net.joaf.mailmessage.modulename">


<div class="awidget full-width">
    <div class="panel">
        <div class="btn-group">
        ${action_buttons!}
        </div>
        <div class="pull-right">
            <a href="${rc.contextPath}/administration.html" class="btn btn-primary"><i
                    class="fa fa-gear fa-fw"></i> <@spring.message 'joaf.back' /></a>
        </div>
        <a href="${rc.contextPath}/administration/message/template.html">Template</a>
        <a href="${rc.contextPath}/administration/message/queue.html">Queue</a>
        <a href="${rc.contextPath}/administration/message/server.html">Server</a>
    </div>

</div>
</@page.layout>