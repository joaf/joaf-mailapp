<#import "/templates/base.ftl" as page />
<@page.layout "net.joaf.mainextensions.menu.modulename">
<div class="awidget full-width">
    <div class="awidget-head">
    </div>
    <div class="awidget-body">
        <form class="form-horizontal" role="form" method="post"
              action="${rc.contextPath}/mailmessage/sender/send.html">

            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.alias' /></label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.messageAlias' 'placeholder="Alias" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.from' /></label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.from' 'placeholder="From" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.to' /></label>

                <div class="col-lg-10">
                    <@spring.formTextarea 'command.to' 'placeholder="To" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.model' /></label>
                <div class="col-lg-10">
                    <@spring.formTextarea 'command.model' 'placeholder="Model" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>

            <button type="submit" class="btn btn-primary"><@spring.message 'joaf.save' /></button>
            <button type="button"
                    onclick="$(this.form).attr('action','${rc.contextPath}/mailmessage/template/cancel.html');$(this.form).submit();"
                    class="btn btn-default">Cancel
            </button>
        </form>
    </div>

</div>
</@page.layout>