<#import "/templates/base.ftl" as page />
<@page.layout "net.joaf.mainextensions.menu.modulename">
<div class="awidget full-width">
    <div class="awidget-head">
        <div class="btn-group">
        <#--<a href="/remoteconnections/edit/0.html" class="btn btn-primary">Create</a>-->
            <#--<a href="/invoice/syncInfakt.html" class="btn btn-info">Infakt sync</a>-->
        <#--<button class="btn btn-success">Center</button>-->
        <#--<button class="btn btn-primary">Right</button>-->
        </div>
    </div>
    <div class="awidget-body">
        <form class="form-horizontal" role="form" method="post"
              action="${rc.contextPath}/administration/mailmessagetemplate/edit/${command.uid}.html">


            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.alias' /></label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.alias' 'placeholder="Alias" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.server' /></label>

                <div class="col-lg-10">
                    <@spring.formSingleSelect 'command.creditialsUid' servers 'placeholder="server" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.title' /></label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.title' 'placeholder="Title" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'joaf.from' /></label>

                <div class="col-lg-10">
                    <@spring.formInput 'command.from' 'placeholder="From" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'net.joaf.mailmessage.bodyhtml' /></label>

                <div class="col-lg-10">
                    <@spring.formTextarea 'command.bodyHtml' 'placeholder="bodyHtml" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label"><@spring.message 'net.joaf.mailmessage.headers' /></label>
                <div class="col-lg-10">
                    <@spring.formTextarea 'command.headers' 'placeholder="bodyHtml" class="form-control"' />
                    <#if spring.status.error>
                        <p><@spring.showErrors "<br>", "color:red" /></p>
                    </#if>
                </div>
            </div>
            <@spring.formHiddenInput 'command.uid' '' />
            <button type="submit" class="btn btn-primary"><@spring.message 'joaf.save' /></button>
            <button type="button"
                    onclick="$(this.form).attr('action','${rc.contextPath}/administration/mailmessagetemplate/cancel/${command.uid}.html');$(this.form).submit();"
                    class="btn btn-default">Cancel
            </button>
        </form>
    </div>

</div>
</@page.layout>