<#import "/templates/base.ftl" as page />
<@page.layout "net.joaf.mainextensions.menu.modulename">


<div class="awidget full-width">
    <div class="panel">
        <div class="btn-group">
            <a href="${rc.contextPath}/administration/mailmessagetemplate/create.html"
               class="btn btn-primary"><@spring.message 'joaf.create' /></a>
        <#--<a href="${rc.contextPath}/administration/menu/export.html" class="btn btn-default">Export</a>-->
        </div>
        <div class="btn-group">
        ${action_buttons!}
        </div>
        <div class="pull-right">
            <a href="${rc.contextPath}/administration.html" class="btn btn-primary"><i
                    class="fa fa-gear fa-fw"></i> <@spring.message 'joaf.back' /></a>
        </div>
        <table class="table table-bordered admin-media ">
            <thead>
            <tr>
                <th><@spring.message 'joaf.alias' /></th>
                <th><@spring.message 'joaf.title' /></th>
                <th><@spring.message 'joaf.uid' /></th>
                <th><@spring.message 'joaf.commands' /></th>
            </tr>
            </thead>
            <tbody>
                <#list elements as element>
                <tr>
                    <td>${element.data.alias}</td>
                    <td>${element.data.title}</td>
                    <td>${element.data.uid}</td>
                    <td>
                        <#list element.webActions as action>
                            <a href="${rc.contextPath}${action.actionUrl}" class="btn btn-xs ${action.buttonStyle}"
                               data-toggle="tooltip" data-placement="bottom"
                               title="<@spring.message action.tooltip />"><span aria-hidden="true"
                                                                                class="glyphicon ${action.iconName}"></span></a>
                        </#list>
                    </td>
                </tr>
                </#list>
            </tbody>
        </table>
    </div>

</div>
</@page.layout>