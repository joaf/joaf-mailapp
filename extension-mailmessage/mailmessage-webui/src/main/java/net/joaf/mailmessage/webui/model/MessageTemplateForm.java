package net.joaf.mailmessage.webui.model;

import net.joaf.base.core.db.EObjectState;
import net.joaf.mailmessage.model.MailMessageTemplate;
import net.joaf.mailmessage.model.enums.EMessageType;
import net.joaf.mailmessage.model.enums.EQueueState;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by cyprian on 21.03.15.
 */
public class MessageTemplateForm implements Serializable {
    private String uid;
    private String alias;
    private String title;
    private String bodyHtml;
    private String bodyText;
    private String creditialsUid;
    private String headers = "";
    private String availableVars;
    private String additionalTo = "";
    private String additionalCC = "";
    private String additionalBCC = "";
    private String from;
    private String replayTo;
    private EMessageType messageType;

    public MessageTemplateForm() {
    }

    public MessageTemplateForm(MailMessageTemplate mailMessageTemplate) {
        this.uid = mailMessageTemplate.getUid();
        this.alias = mailMessageTemplate.getAlias();
        this.title = mailMessageTemplate.getTitle();
        this.bodyHtml = mailMessageTemplate.getBodyHtml();
        this.bodyText = mailMessageTemplate.getBodyText();
        this.creditialsUid = mailMessageTemplate.getCreditialsUid();
        this.from = mailMessageTemplate.getFrom();
        this.additionalBCC = StringUtils.join(mailMessageTemplate.getAdditionalBCC(), "\n");
        this.headers = StringUtils.join(mailMessageTemplate.getHeaders().entrySet().stream().map(x -> x.getKey() + ":" + x.getValue()).collect(Collectors.toList()), "\n");
    }

    public MailMessageTemplate toMailMessageTemplate() {
        MailMessageTemplate mailMessageTemplate = new MailMessageTemplate();
        mailMessageTemplate.setUid(this.getUid());
        mailMessageTemplate.setAlias(this.getAlias());
        mailMessageTemplate.setTitle(this.getTitle());
        mailMessageTemplate.setBodyHtml(this.getBodyHtml());
        mailMessageTemplate.setBodyText(this.getBodyText());
        mailMessageTemplate.setCreditialsUid(this.getCreditialsUid());
        mailMessageTemplate.setFrom(this.getFrom());
        mailMessageTemplate.getAdditionalBCC().addAll(Arrays.asList(StringUtils.split(this.getAdditionalBCC(), "\n")));
        mailMessageTemplate.getHeaders().putAll(Arrays.asList(StringUtils.split(this.getHeaders(), "\n")).stream().map(x -> {
            String[] entry = x.split(":");
            if (entry.length != 2) {
                throw new IllegalArgumentException("IllegalArgument "+x);
            }
            return new HashMap.SimpleEntry<>(entry[0], entry[1]);
        }).collect(Collectors.toMap(HashMap.SimpleEntry::getKey, HashMap.SimpleEntry::getValue)));
        return mailMessageTemplate;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBodyHtml() {
        return bodyHtml;
    }

    public void setBodyHtml(String bodyHtml) {
        this.bodyHtml = bodyHtml;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public String getCreditialsUid() {
        return creditialsUid;
    }

    public void setCreditialsUid(String creditialsUid) {
        this.creditialsUid = creditialsUid;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public String getAvailableVars() {
        return availableVars;
    }

    public void setAvailableVars(String availableVars) {
        this.availableVars = availableVars;
    }

    public String getAdditionalTo() {
        return additionalTo;
    }

    public void setAdditionalTo(String additionalTo) {
        this.additionalTo = additionalTo;
    }

    public String getAdditionalCC() {
        return additionalCC;
    }

    public void setAdditionalCC(String additionalCC) {
        this.additionalCC = additionalCC;
    }

    public String getAdditionalBCC() {
        return additionalBCC;
    }

    public void setAdditionalBCC(String additionalBCC) {
        this.additionalBCC = additionalBCC;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getReplayTo() {
        return replayTo;
    }

    public void setReplayTo(String replayTo) {
        this.replayTo = replayTo;
    }

    public EMessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(EMessageType messageType) {
        this.messageType = messageType;
    }
}
