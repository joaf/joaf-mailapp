package net.joaf.mailmessage.webui.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafDatabaseException;
import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.subject.SubjectCardDataHelper;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.mailmessage.commands.RegisterSendMessageRequestCommand;
import net.joaf.mailmessage.commands.UpdateAllESMessageCommand;
import net.joaf.mailmessage.commands.handlers.RegisterSendMessageRequestCommandHandler;
import net.joaf.mailmessage.commands.handlers.UpdateAllESMessageCommandHandler;
import net.joaf.mailmessage.model.MailMessage;
import net.joaf.mailmessage.queries.MessageQuery;
import net.joaf.mailmessage.webui.helpers.MessageActionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Optional;

/**
 * Created by cyprian on 18.03.15.
 */
@Controller
@RequestMapping("/mailmessage/queue")
public class MessageQueueController {

	@Autowired
	private MessageQuery query;

	@Autowired
	private CommandGateway commandGateway;

	@RequestMapping
	public String list(Model model, HttpSession session) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
		try {
			PageredResult<MailMessage> list = query.list(
					new PageredRequest(Optional.<Integer>empty(), Optional.<Integer>empty(), Optional.of(EObjectState.ACCEPTED),
							sessionContext.getSubjectCardDataId()));
			list.wrap(this::createActions);
			model.addAttribute("elements", list.getElements());
		} catch (JoafDatabaseException e) {
			e.printStackTrace();
		}
		return "/freemarker/mailmessage/queue/list";
	}

	private void createActions(WrapperWDTO<MailMessage> x) {
		x.getData().getActions().forEach(action -> x.getWebActions().add(
				MessageActionHelper.createActionDetails(action, x.getData().getUid(), x.getData().getTemplateAlias())));
	}

	@RequestMapping("/dashboard")
	public String dashboard(HttpSession session) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
		return "/freemarker/mailmessage/queue/dashboard";
	}

	@RequestMapping("/updatees")
	public String updatees(HttpSession session, RedirectAttributes redirectAttributes, Principal principal) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
		try {
			commandGateway.send(new UpdateAllESMessageCommand(principal.getName()), UpdateAllESMessageCommandHandler.class);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave");
		} catch (Exception e) {
			e.printStackTrace();
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
		}
		return "redirect:/mailmessage/queue/dashboard";
	}

	protected String getSubjectCardDataUid(SessionContext sessionContext) {
		return sessionContext.getSubjectCardDataId();
	}

}
