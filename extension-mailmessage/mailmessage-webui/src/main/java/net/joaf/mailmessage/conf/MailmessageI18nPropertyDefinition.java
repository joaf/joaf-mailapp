package net.joaf.mailmessage.conf;

import net.joaf.base.language.utils.I18nPropertyDefinition;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Created by cyprian on 15.03.15.
 */
@Component
public class MailmessageI18nPropertyDefinition implements I18nPropertyDefinition {
    @Override
    public List<String> propertyFiles() {
        return Arrays.asList("classpath:i18n/mailmessage");
    }
}
