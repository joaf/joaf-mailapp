package net.joaf.mailmessage.webui.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.mailmessage.model.MailMessageTemplate;
import net.joaf.mailmessage.commands.*;
import net.joaf.mailmessage.commands.handlers.*;
import net.joaf.mailmessage.queries.MessageTemplateQuery;
import net.joaf.mailmessage.webui.helpers.MessageTemplateActionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.Optional;

/**
 * Created by cyprian on 14.03.15.
 */
@Controller
@RequestMapping("/administration/message/template")
public class MessageTemplateAdminController extends MessageTemplateController {

    protected String getSubjectCardDataUid(SessionContext sessionContext) {
        return "app";
    }

    protected String getBackPath() {
        return "/administration.html";
    }

    protected String getControllerContextPath() {
        return "/administration/message/template";
    }

	protected String getUrl(String name) {
        return "/administration/message/template" + name + ".html";
    }
}
