package net.joaf.mailmessage.webui.model;

import net.joaf.base.core.utils.TypeUtils;
import net.joaf.mailmessage.model.MailMessageRequest;

import java.io.Serializable;

/**
 * Created by cyprian on 21.03.15.
 */
public class MessageSendForm implements Serializable {
    private String model = "";
    private String messageAlias;
    private String subjectUid;
    private String from;
    private String to = "";
    private String cc = "";
    private String bcc = "";

    public MessageSendForm() {
    }

    public MessageSendForm(MailMessageRequest request) {
        this.messageAlias = request.getMessageAlias();
        this.subjectUid = request.getSubjectUid();
        this.from = request.getFrom();
        this.model = TypeUtils.mapToString(request.getModel());
        this.to = TypeUtils.setToString(request.getTo());
        this.cc = TypeUtils.setToString(request.getCc());
        this.bcc = TypeUtils.setToString(request.getBcc());
    }

    public MailMessageRequest toMailMessageRequest() {
        MailMessageRequest mailMessageRequest = new MailMessageRequest();
        mailMessageRequest.setMessageAlias(this.getMessageAlias());
        mailMessageRequest.setSubjectUid(this.getSubjectUid());
        mailMessageRequest.setFrom(this.getFrom());
        mailMessageRequest.getModel().putAll(TypeUtils.stringToMap(this.getModel()));
        mailMessageRequest.getTo().addAll(TypeUtils.stringToSet(this.getTo()));
        mailMessageRequest.getCc().addAll(TypeUtils.stringToSet(this.getCc()));
        mailMessageRequest.getBcc().addAll(TypeUtils.stringToSet(this.getBcc()));
        return mailMessageRequest;
    }


    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMessageAlias() {
        return messageAlias;
    }

    public void setMessageAlias(String messageAlias) {
        this.messageAlias = messageAlias;
    }

    public String getSubjectUid() {
        return subjectUid;
    }

    public void setSubjectUid(String subjectUid) {
        this.subjectUid = subjectUid;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }
}
