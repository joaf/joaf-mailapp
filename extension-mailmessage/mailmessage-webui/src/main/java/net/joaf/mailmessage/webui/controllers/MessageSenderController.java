package net.joaf.mailmessage.webui.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.subject.SubjectCardDataHelper;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.mailmessage.commands.RegisterSendMessageRequestCommand;
import net.joaf.mailmessage.commands.handlers.RegisterSendMessageRequestCommandHandler;
import net.joaf.mailmessage.commands.handlers.SaveMessageTemplateCommandHandler;
import net.joaf.mailmessage.model.MailMessageRequest;
import net.joaf.mailmessage.queries.MessageTemplateQuery;
import net.joaf.mailmessage.webui.model.MessageSendForm;
import net.joaf.mailmessage.webui.model.MessageTemplateForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
@Controller
@RequestMapping("/mailmessage/sender")
public class MessageSenderController {

	@Autowired
	private MessageTemplateQuery query;

	@Autowired
	private CommandGateway commandGateway;

	@RequestMapping
	public String form(Model model) {
		return formByAlias(null, model);
	}

	@RequestMapping("/{alias}")
	public String formByAlias(@PathVariable String alias, Model model) {
        MessageSendForm command = new MessageSendForm();
		command.setMessageAlias(alias);
        model.addAttribute("command", command);
		return "/freemarker/mailmessage/sender/message";
	}


	@RequestMapping(value = "/send", method = RequestMethod.POST)
	public String prepareSend(@Valid @ModelAttribute MessageSendForm request, Model model, HttpSession session, RedirectAttributes redirectAttributes, Principal principal, HttpServletRequest httpRequest)
			throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
		SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
		try {
			commandGateway.send(new RegisterSendMessageRequestCommand(request.toMailMessageRequest(), principal.getName(), getSubjectCardDataUid(sessionContext),httpRequest), RegisterSendMessageRequestCommandHandler.class);
			WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave");
			return "redirect:/mailmessage/sender";
		} catch (Exception e) {
			model.addAttribute("command", request);
			e.printStackTrace();
			WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
			return "/freemarker/mailmessage/sender/message";
		}

	}
	protected String getSubjectCardDataUid(SessionContext sessionContext) {
		return sessionContext.getSubjectCardDataId();
	}

}
