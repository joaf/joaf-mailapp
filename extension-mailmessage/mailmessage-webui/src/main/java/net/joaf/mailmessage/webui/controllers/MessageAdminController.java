package net.joaf.mailmessage.webui.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by cyprian on 02.04.15.
 */
@Controller
@RequestMapping("/administration/message")
public class MessageAdminController {

    @RequestMapping("")
    public String index() {
        return getViewitem("/index");
    }

    protected String getViewitem(String name) {
        return "/freemarker/mailmessage/admin" + name;
    }

    protected String getRedirect(String name) {
        return "redirect:" + getUrl(name);
    }

    protected String getUrl(String name) {
        return "/administration/message" + name + ".html";
    }
}
