package net.joaf.mailmessage.webui.controllers;

import net.joaf.base.core.cqrs.CommandGateway;
import net.joaf.base.core.cqrs.PageredRequest;
import net.joaf.base.core.cqrs.PageredResult;
import net.joaf.base.core.db.EObjectState;
import net.joaf.base.core.error.JoafNoSubjectContextException;
import net.joaf.base.core.session.SessionContext;
import net.joaf.base.core.subject.SubjectCardDataHelper;
import net.joaf.base.core.utils.WebFlashUtil;
import net.joaf.base.core.web.model.WrapperWDTO;
import net.joaf.mailmessage.commands.CancelMessageTemplateCommand;
import net.joaf.mailmessage.commands.CreateMessageTemplateCommand;
import net.joaf.mailmessage.commands.SaveMessageTemplateCommand;
import net.joaf.mailmessage.commands.TrashMessageTemplateCommand;
import net.joaf.mailmessage.commands.UndoTrashMessageTemplateCommand;
import net.joaf.mailmessage.commands.handlers.CancelMessageTemplateCommandHandler;
import net.joaf.mailmessage.commands.handlers.CreateMessageTemplateCommandHandler;
import net.joaf.mailmessage.commands.handlers.SaveMessageTemplateCommandHandler;
import net.joaf.mailmessage.commands.handlers.TrashMessageTemplateCommandHandler;
import net.joaf.mailmessage.commands.handlers.UndoTrashMessageTemplateCommandHandler;
import net.joaf.mailmessage.model.MailMessageTemplate;
import net.joaf.mailmessage.queries.MessageTemplateQuery;
import net.joaf.mailmessage.webui.helpers.MessageTemplateActionHelper;
import net.joaf.mailmessage.webui.model.MessageTemplateForm;
import net.joaf.mainextensions.connection.model.ExternalConnection;
import net.joaf.mainextensions.connection.queries.ExternalConnectionQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;
import java.util.*;

/**
 * Created by cyprian on 14.03.15.
 */
@Controller
@RequestMapping("/mailmessage/template")
public class MessageTemplateController {

    @Autowired
    private MessageTemplateQuery query;

    @Autowired
    private ExternalConnectionQuery externalConnectionQuery;

    @Autowired
    private CommandGateway commandGateway;

    //	@Autowired
    //	private ImportExportHelper importExportHelper;

    private static final Logger log = LoggerFactory.getLogger(MessageTemplateController.class);

    @RequestMapping
    public String list(HttpSession session, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            PageredRequest pageredRequest = new PageredRequest(Optional.empty(), Optional.empty(), Optional.of(EObjectState.ACCEPTED), getSubjectCardDataUid(sessionContext));
            PageredResult<MailMessageTemplate> all = query.findAll(pageredRequest);
            all.wrap(this::createActions);
            model.addAttribute("elements", all.getElements());
        } catch (Exception e) {
            model.addAttribute("elements", Collections.emptyList());
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            log.debug("error", e);
        }
        updateModelWithPaths(model);
        return getViewitem("/list");
    }

    private void createActions(WrapperWDTO<MailMessageTemplate> x) {
        x.getData().getActions().forEach(action -> x.getWebActions().add(MessageTemplateActionHelper.createActionDetails(action, x.getData().getUid(), x.getData().getAlias(), getControllerContextPath())));
    }

    @RequestMapping("/create")
    public String create(HttpSession session, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
			MailMessageTemplate command = (MailMessageTemplate) commandGateway
					.send(new CreateMessageTemplateCommand(principal.getName(), getSubjectCardDataUid(sessionContext)), CreateMessageTemplateCommandHandler.class);
			return getRedirect("/edit/" + command.getUid());
		} catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") String id, HttpSession session, RedirectAttributes redirectAttributes, Model model) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
        SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
		try {
			MailMessageTemplate mailMessageTemplate = query.findOne(id);
            MessageTemplateForm command = new MessageTemplateForm(mailMessageTemplate);
            model.addAttribute("command", command);
            updateModelWithServers(model, sessionContext);
            updateModelWithPaths(model);
			return getViewitem("/edit");
		} catch (Exception e) {
            log.debug("error", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            return getRedirect("");
        }
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public String saveItem(@Valid @ModelAttribute("command") MessageTemplateForm command, @PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
		SessionContext sessionContext = SessionContext.loadFromSession(session);
        SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            MailMessageTemplate mailMessageTemplate = command.toMailMessageTemplate();
            commandGateway.send(new SaveMessageTemplateCommand(mailMessageTemplate, principal.getName()), SaveMessageTemplateCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successsave");
            return getRedirect("");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
            updateModelWithServers(model, sessionContext);
            updateModelWithPaths(model);
            return getViewitem("/edit");
        }
    }

    @RequestMapping("/trash/{uid}")
    public String trash(@PathVariable("uid") String uid, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            commandGateway.send(new TrashMessageTemplateCommand(uid, principal.getName(), getSubjectCardDataUid(sessionContext)), TrashMessageTemplateCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successtrash", httpRequest.getContextPath() + getUrl("/undotrash/" + uid));
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping("/undotrash/{id}")
    public String undotrash(@PathVariable("id") String id, HttpSession session, HttpServletRequest httpRequest, RedirectAttributes redirectAttributes, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            commandGateway.send(new UndoTrashMessageTemplateCommand(id, principal.getName(), getSubjectCardDataUid(sessionContext)), UndoTrashMessageTemplateCommandHandler.class);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.message.successundotrash", "1");
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addMessage(redirectAttributes, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    @RequestMapping(value = "/cancel/{uid}", method = RequestMethod.POST)
    public String cancel(@PathVariable("uid") String uid, HttpSession session, Principal principal, Model model) throws JoafNoSubjectContextException {
        SessionContext sessionContext = SessionContext.loadFromSession(session);
        SubjectCardDataHelper.checkSubjectUid(getSubjectCardDataUid(sessionContext));
        try {
            commandGateway.send(new CancelMessageTemplateCommand(uid, principal.getName(), getSubjectCardDataUid(sessionContext)), CancelMessageTemplateCommandHandler.class);
        } catch (Exception e) {
            log.error("", e);
            WebFlashUtil.addModelMessage(model, "joaf.message.notification.title", "joaf.error.body", e.getLocalizedMessage());
        }
        return getRedirect("");
    }

    protected void updateModelWithServers(Model model, SessionContext sessionContext) {
        Map<String, String> serversMap = new HashMap<>();
        try {
            PageredResult<ExternalConnection> servers = externalConnectionQuery.findForType(
                    new PageredRequest(Optional.<Integer>empty(), Optional.<Integer>empty(), Optional.of(EObjectState.ACCEPTED),
                            getSubjectCardDataUid(sessionContext)), "smtp");

            for (ExternalConnection connection : servers.getResult()) {
                serversMap.put(connection.getUid(), connection.getName());
            }
        } catch (Exception e) {
            log.error("Error reading servers", e);
        }
        model.addAttribute("servers", serversMap);
    }

    protected String getSubjectCardDataUid(SessionContext sessionContext) {
        return sessionContext.getSubjectCardDataId();
    }

    protected void updateModelWithPaths(Model model) {
        model.addAttribute("backPath", getBackPath());
        model.addAttribute("controllerContextPath", getControllerContextPath());
    }

    protected String getBackPath() {
        return "/";
    }

    protected String getControllerContextPath() {
        return "/mailmessage/template";
    }

    protected String getViewitem(String name) {
        return "/freemarker/mailmessage/template" + name;
    }

	protected String getRedirect(String name) {
        return "redirect:" + getUrl(name);
    }

	protected String getUrl(String name) {
        return "/mailmessage/template" + name + ".html";
    }
}
