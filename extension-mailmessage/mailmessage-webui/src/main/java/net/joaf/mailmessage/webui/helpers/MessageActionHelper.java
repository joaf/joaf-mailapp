package net.joaf.mailmessage.webui.helpers;

import net.joaf.base.core.web.model.WebActionDetails;
import net.joaf.mailmessage.model.enums.EMessageAction;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public class MessageActionHelper {
	public static WebActionDetails createActionDetails(EMessageAction action, String uid, String alias) {
		WebActionDetails webActionDetails = new WebActionDetails();
		switch (action) {
		case RESEND:
			webActionDetails.setButtonStyle("btn-success");
			webActionDetails.setActionUrl("/mailmessage/queue/resend/" + uid + ".html");
			webActionDetails.setIconName("glyphicon-share");
			webActionDetails.setTooltip("joaf.send");
			break;
		default:
			webActionDetails = null;
			break;
		}
		return webActionDetails;
	}
}
