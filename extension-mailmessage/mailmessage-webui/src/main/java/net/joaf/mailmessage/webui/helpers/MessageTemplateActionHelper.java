package net.joaf.mailmessage.webui.helpers;

import net.joaf.base.core.web.model.WebActionDetails;
import net.joaf.mailmessage.model.enums.EMessageTemplateAction;

/**
 * Created by cyprian on 07.03.15.
 */
public class MessageTemplateActionHelper {

    public static WebActionDetails createActionDetails(EMessageTemplateAction action, String uid, String alias, String controllerContextPath) {
        WebActionDetails webActionDetails = new WebActionDetails();
        switch (action) {
            case EDIT:
                webActionDetails.setButtonStyle("btn-primary");
                webActionDetails.setActionUrl(controllerContextPath+"/edit/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-edit");
                webActionDetails.setTooltip("joaf.edit");
                break;
            case TRASH:
                webActionDetails.setButtonStyle("btn-danger");
                webActionDetails.setActionUrl(controllerContextPath+"/trash/" + uid + ".html");
                webActionDetails.setIconName("glyphicon-trash");
                webActionDetails.setTooltip("joaf.trash");
                break;
			case PREPARE_SEND:
				webActionDetails.setButtonStyle("btn-success");
				webActionDetails.setActionUrl("/mailmessage/sender/" + alias + ".html");
				webActionDetails.setIconName("glyphicon-share");
				webActionDetails.setTooltip("joaf.send");
				break;
            default:
                webActionDetails = null;
                break;
        }
        return webActionDetails;
    }
}
