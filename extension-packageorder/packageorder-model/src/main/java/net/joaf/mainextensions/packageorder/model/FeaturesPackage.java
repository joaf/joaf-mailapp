package net.joaf.mainextensions.packageorder.model;

import net.joaf.base.core.db.entity.JoafEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Cyprian Śniegota
 * @since 1.0
 */
public class FeaturesPackage extends JoafEntity implements Serializable {
	private String alias;
	private String title;
	private Map<String, String> settings = new HashMap<>();
	private String description;
	private BigDecimal netPrice;

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Map<String, String> getSettings() {
		return settings;
	}

	public void setSettings(Map<String, String> settings) {
		this.settings = settings;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getNetPrice() {
		return netPrice;
	}

	public void setNetPrice(BigDecimal netPrice) {
		this.netPrice = netPrice;
	}
}
